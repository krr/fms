parser grammar AtomParser;

options { tokenVocab=AtomLexer; }


atom : ID (LPAREN (arg COMMA)* arg RPAREN)?;

arg : INTLIT #intLit
    | STRING #stringLit
    | ID (LPAREN (arg COMMA)* arg RPAREN)? #herbrand
    | LPAREN ((arg COMMA)+ arg)? RPAREN #tuple
    ;
