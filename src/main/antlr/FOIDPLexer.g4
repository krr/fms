lexer grammar FOIDPLexer;


//Console
TYPE               : ':t';

// Whitespace
NEWLINE            : ('\r\n' | '\r' | '\n') -> skip;
WS                 : [\t ]+ -> skip ;
COMMENT            : '/*' .*? '*/' -> skip;
LINE_COMMENT       : '//' ~[\r\n]* -> skip;
DOUBLEDOT          : '..';
ENDOFSTATEMENT     : '.' ;

SCRIPT             : '#script(' ((.|'\n')*?) '#end.';

// Keywords
LET                : 'let' ;
IN                 : 'in';
BY                 : 'by';
IMPORT             : 'import';
SUBSET             : 'subset of';
ELEMENT            : 'element of';
FUNCTION           : 'function';
PREDICATE          : 'predicate';
PROPOSITION        : 'proposition';
EXTERNAL           : 'external';
FROM               : 'from';
TO                 : 'to';
CASE               : 'case';
OF                 : 'of';
CONSTRUCTOR        : 'constructor';
BSLASH             : '\\';
HASHTAG            : '#';
IF                 : 'if';
THEN               : 'then';
ELSE               : 'else';

// Literals
INTLIT             : ('0'|[1-9][0-9]*) ;
// Operators
TYPED              : '::';
COMMA              : ',' ;
PLUS               : '+' ;
MINUS              : '-' ;
OR                 : '|' ;
AND                : '&' ;
NOT                : '~' ;
TIMES              : '*' ;
DIVISION           : '/' ;
MODULO             : '%' ;
ASSIGN             : ':=';
STRICTASSIGN       : '::=';
LT            	   : '<' ;
LEQ                : '=<';
GT                 : '>' ;
GEQ                : '>=';
EQ                 : '=' ;
NEQ                : '~=';
LPAREN             : '(' ;
RPAREN             : ')' ;
LBRACE             : '{' ;
RBRACE             : '}' ;
LSQUARE            : '[' ;
RSQUARE            : ']' ;
MAPSTO             : '->' ;
BINDS              : '<-' ;
IMPL               : '=>' ;
RIMPL              : '<=' ;
IFF                : '<=>';
DONTCARE           : '_';
EOCASE             : ';';
BACKTICK           : '`';
COMPOSE            : '<|';
RCOMPOSE           : '|>';

// Identifiers
BUILTIN            : ('$'[_]*[a-zA-Z][A-Za-z0-9_]*) | '!' | '?' ;
DOLLAR             : '$';
ID                 : [_]*[a-zA-Z][A-Za-z0-9_]* ;
STRING             : '"' .*? '"'; //TODO add escaping
