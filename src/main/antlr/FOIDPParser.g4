parser grammar FOIDPParser;

options { tokenVocab=FOIDPLexer; }


console : TYPE expr EOF        #typeQuery
        | foidpFile EOF        #runQuery
        ;

foidpFile : (directive|script)* libraryImport* line* EOF;

foidpLib  : libraryImport* (assignment ENDOFSTATEMENT)* EOF;

directive : HASHTAG ID INTLIT ENDOFSTATEMENT;
script : SCRIPT;

libraryImport : IMPORT STRING ENDOFSTATEMENT;

line : statement ENDOFSTATEMENT ;

statement : bind        #bindStat
          | expr        #exprStat
          ;
bind : assignment  # assignBind
     | declaration # declBind
     ;

declaration : ID DIVISION INTLIT TYPED CONSTRUCTOR       #constrDecl
            | ID TYPED EXTERNAL type                     #externDecl
            | ID domain                                  #openDecl
            ;

domain : TYPED SUBSET expr            # setDomain
       | TYPED ELEMENT expr           # elemDomain
       | DIVISION INTLIT TYPED FUNCTION TO expr       # funcDomain
       | DIVISION INTLIT TYPED FUNCTION BY expr       # lamFuncDomain
       | TYPED PROPOSITION            # propDomain
       | DIVISION INTLIT TYPED PREDICATE              # predDomain
       ;

assignment : name=ID pattern* ASSIGN expr #funcAssignment
         | name=pattern ASSIGN expr       #patternAssignment
         | name=ID TYPED type             #typedAssignment
         | name=ID HASHTAG ID             #pragmaAssignment
         | name=ID pattern* guardExp+     #guardAssignment
         | name=ID STRICTASSIGN expr      #strictAssignStat
         ;

guardExp  : OR cond=expr ASSIGN val=expr EOCASE;

expr:  expr DOLLAR expr                     #dollarApplication
    |  expr1                                #nonDollar;

expr1: expr1 op=(COMPOSE | RCOMPOSE) expr1    # opExpr
     | expr1 op=(TIMES | DIVISION | MODULO) expr1 # opExpr
     | expr1 op=(PLUS | MINUS) expr1           # opExpr
     | expr1 op=(LEQ | GEQ | LT | GT) expr1    # opExpr
     | expr1 op=(EQ | NEQ) expr1               # opExpr
     | expr1 op=AND expr1                      # opExpr
     | expr1 op=OR expr1                       # opExpr
     | expr1 op=(IMPL|RIMPL) expr1             # opExpr
     | expr1 op=IFF expr1                      # opExpr
     | expr1 BACKTICK ID BACKTICK expr1        # infixExpr
     | expr1 TYPED type                       # typedExpr
     | expr2                                 # simpleExpr
     ;

expr2: MINUS expr2                           # unaryMinExpr
     | NOT expr2                             # notExpr
     | expr3                                 # baseExpr
     ;

expr3: INTLIT                                # intLit
     | STRING                                # stringLit
     | ID                                    # id
     | BUILTIN                               # builtinExpr
     | LPAREN expr RPAREN                    # parenExpression
     | LPAREN RPAREN                         # unitExpression
     | LPAREN (expr COMMA)+ expr RPAREN      # tupleExpression
     | IF expr THEN expr ELSE expr           # iteExpr
     | BSLASH pattern+ MAPSTO expr           # abstraction
     | LET bindings IN expr                  # let
     | CASE expr OF alt+                     # caseExpr
     | set                                   # setExpr
     | LBRACE expr DOUBLEDOT expr RBRACE     # rangeExpr
     | expr3 expr3                           # application
     ;

bindings : (assignment EOCASE)* assignment;

alt : pattern MAPSTO expr EOCASE;

pattern : ID                       #varPattern
        | LPAREN RPAREN            #unitPattern
        | LPAREN pattern RPAREN    #parenPattern
        | LPAREN (pattern COMMA)+ pattern RPAREN #tuplePattern
        | ID LSQUARE pattern* RSQUARE #constructedPattern
        | DONTCARE                 #anonymPattern
        | INTLIT                   #intLitPattern
        | STRING                   #stringLitPattern
        | op=(LEQ|GEQ|LT|GT|EQ|NEQ) INTLIT #intCompPattern
        ;

condition  : pattern BINDS expr     #bindCond
           | pattern ASSIGN expr    #assCond
           | expr                   #exprCond
           ;

set : LBRACE (expr (COMMA*))* (OR OR (condition (COMMA*))*)? RBRACE;

type : ID                               #varType
     | LPAREN type RPAREN               #parenType
     | LPAREN RPAREN                    #unitType
     | LPAREN (type COMMA)+ type RPAREN #tupleType
     | type type                        #applicationType
     |<assoc=right> type MAPSTO type    #arrowType
     ;
