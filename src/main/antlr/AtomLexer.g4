lexer grammar AtomLexer;

COMMA              : ',' ;
LPAREN             : '(' ;
RPAREN             : ')' ;
STRING             : '"' .*? '"'; //TODO add escaping
INTLIT             : ('-'?)('0'|[1-9][0-9]*) ;
ID                 : [#_a-z][#A-Za-z0-9_]* ;
