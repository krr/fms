package fms.optimiser

import fms.globalLog
import fms.fml.abstract.BoundVar
import fms.fml.abstract.DirectedGraph
import fms.fml.abstract.FreeVar
import fms.fml.abstract.Node
import fms.fml.abstract.Nodes
import fms.fml.abstract.Var
import fms.fml.abstract.tarjan
import fms.fml.expression.Bind
import fms.fml.expression.Exp
import fms.fml.expression.Let
import fms.fml.expression.Scoped
import fms.fml.expression.V
import fms.fml.expression.VarT
import fms.fml.expression.emptyInstantiate
import fms.fml.expression.push

object TarjanLet : ExpressionTransformer() {
    override fun <A> visitLet(exp: Let<A>): Exp<A> {
        return makeSingle(exp.binds, visitWithScoped(exp.exp))
    }

    fun <A> makeSingle(binds: List<Bind<A>>, exp: Scoped<A>): Exp<A> {
        val newBinds = binds.analyzeDependencies(exp)
        globalLog.trace { "Dependency Result: ${newBinds.map { it.map { it.name } }}" }
        val transformed = newBinds.foldRight(exp) { component, curExp ->
            val componentNames = component.map { it.name }

            fun Scoped<A>.instantiator(): Scoped<Var<VarT, A>> {
                val out: Exp<Var<VarT, Var<VarT, A>>> = unscope.bind<Var<VarT, Var<VarT, A>>> {
                    if (it is BoundVar && componentNames.contains(it.b.name)) {
                        val out = BoundVar<VarT, Exp<Var<VarT, A>>>(VarT(it.b.name, componentNames.indexOf(it.b.name)))
                                as Var<VarT, Exp<Var<VarT, A>>>
                        V(out).push()
                    } else {
                        V(FreeVar(it))
                    }
                }
                return Scoped.make(out)
            }

            val newExp = curExp.instantiator()
            val instBinds = component.map { it.mapExp { it.instantiator() } }
            Scoped.make(Let(instBinds, newExp))
        }
        globalLog.trace { "Transformed: $transformed" }
        return transformed.emptyInstantiate()
    }

    fun <A> List<Bind<A>>.analyzeDependencies(e: Scoped<A>): List<List<Bind<A>>> {
        val nodes = this.map { it.name to Node(it) }.toMap()
        val dependencies = mutableMapOf<Node<Bind<A>>, Nodes<Bind<A>>>()
        nodes.values.forEach {
            dependencies[it] = listOf()
        }
        this.forEach { bind ->
            val bindNode = nodes[bind.name]!!
            val frees = bind.exp.boundVars()
            frees.forEach { free ->
                val freeNode = nodes[free.name]
                if (freeNode != null) {
                    dependencies.compute(bindNode,
                            { _, curVal -> curVal!! + freeNode })
                }
            }
        }
        val needed = e.boundVars().map { it.name } + this.filter { it.pragma.strict }.map { it.name }
        val graph = DirectedGraph(nodes.filterKeys { needed.contains(it) }.values.toList(), dependencies)
        val scss = tarjan(graph).map { it.map { it.n } }
        return scss
    }
}