package fms.optimiser

import fms.globalLog
import fms.fml.abstract.BoundVar
import fms.fml.abstract.Var
import fms.fml.expression.Application
import fms.fml.expression.Bind
import fms.fml.expression.BindPragma
import fms.fml.expression.Case
import fms.fml.expression.Exp
import fms.fml.expression.Injection
import fms.fml.expression.Lambda
import fms.fml.expression.Let
import fms.fml.expression.Scoped
import fms.fml.expression.V
import fms.fml.expression.VarT
import fms.fml.expression.emptyInstantiate

object InlineLet : ExpressionTransformer() {
    override tailrec fun <A> visitLet(exp: Let<A>): Exp<A> {
        val orig = visitWithScoped(exp.exp)

        val usedCount = exp.countBinderUsage()
        val inliner = mutableMapOf<String, Scoped<A>>()
        val keeps = mutableListOf<Bind<A>>()

        // Partition into inliner and keeps
        for (bind in exp.binds) {
            val totalUsages = usedCount[bind.name] ?: 0
            if ((bind.pragma.inline == true || (bind.pragma.inline == null && totalUsages <= 1) || bind.exp.unscope is Injection<*, *>) &&
                !bind.pragma.strict) {
                inliner[bind.name] = bind.exp
            } else {
                keeps.add(bind)
            }
        }
        val keepvars = keeps.map { it.name }
        val keepBinds = keeps
                .map { it.mapExp { it.applyInstantiator(inliner, keepvars) } }
                .map { it.mapExp(this::visitWithScoped) }

        if (inliner.isEmpty()) {
            globalLog.trace { "Nothing left to inline" }
            return Let(keepBinds, orig)
        }
        val inlined = visitWithScoped(orig.applyInstantiator(inliner, keepvars))

        if (keeps.isEmpty()) {
            globalLog.trace { "InlineLet Result: $inlined" }
            return inlined.emptyInstantiate()
        }
        val out = Let(keepBinds, inlined)
        globalLog.trace { "InlineLet Result: $out" }
        return visitLet(out)
    }

    override fun <A> visitApplication(exp: Application<A>): Exp<A> {
        val f = visit(exp.f)
        if (f is Lambda) {
            val c: Scoped<A> = exp.x.emptyScope()
            val b = Bind(f.v, c, null, BindPragma())
            val nLet = Let(listOf(b), f.f)
            return visit(nLet)
        }
        if (f is Case && f.alts.size <= 1) {
            val newAlts = f.alts.map {
                it.copy(exp = Scoped.make(Application(it.exp.unscope, exp.x.emptyScoped())))
            }
            return visit(f.copy(alts = newAlts))
        }
        return super.visitApplication(exp)
    }

    fun <A> Let<A>.countBinderUsage(): MutableMap<String, Int> {
        val usedCount = mutableMapOf<String, Int>()
        fun count(name: String) {
            usedCount.compute(name) { _, u -> u?.let { it + 1 } ?: 1 }
        }
        for (bind in this.binds) {
            bind.exp.instantiate {
                count(it.name)
                this
            }
        }
        exp.instantiate {
            count(it.name)
            this
        }
        globalLog.trace { "Inliner counter: $usedCount" }
        return usedCount
    }
}

/**
 * This method replaces the Bound Variables, withSafety the method in the inliner method
 * If the Bound Variable does not occur, it rebinds it withSafety an index taken from newVarList
 */
fun <A> Scoped<A>.applyInstantiator(inliner: Map<String, Scoped<A>>, newVarList: List<String>): Scoped<A> {
    fun Var<VarT, A>.instantiator(): Exp<Var<VarT, A>> {
        if (this is BoundVar) {
            return if (inliner.containsKey(this.b.name)) {
                ExpressionTransformer.duplicate(inliner[this@instantiator.b.name]!!.applyInstantiator(inliner, newVarList).unscope)
            } else {
                V(BoundVar(VarT(this.b.name, newVarList.indexOf(this.b.name))))
            }
        }
        return V(this)
    }
    return Scoped.make(this.unscope.bind(Var<VarT, A>::instantiator))
}