package fms.optimiser

import fms.fml.expression.Alt
import fms.fml.expression.Application
import fms.fml.expression.Bind
import fms.fml.expression.Case
import fms.fml.expression.Exp
import fms.fml.expression.HerbrandExp
import fms.fml.expression.Injection
import fms.fml.expression.Lambda
import fms.fml.expression.LangBuiltin
import fms.fml.expression.Let
import fms.fml.expression.OutputExp
import fms.fml.expression.Scoped
import fms.fml.expression.SetExp
import fms.fml.expression.V

abstract class ExpressionTransformer {
    companion object {
        fun <A> duplicate(exp: Exp<A>): Exp<A> {
            val trans = object : ExpressionTransformer() {}
            return trans.visit(exp)
        }
    }

    @Suppress("UNCHECKED_CAST")
    open fun <A> visit(exp: Exp<A>): Exp<A> = when (exp) {
        is V -> visitV(exp)
        is Application -> visitApplication(exp)
        is Lambda -> visitLambda(exp)
        is Let -> visitLet(exp)
        is Injection<*, *> -> visitInjection(exp as Injection<A, Any>)
        is Case -> visitCase(exp)
        is LangBuiltin -> visitLangBuiltin(exp)
        is SetExp -> visitSet(exp)
        is HerbrandExp -> visitHerbrandExp(exp)
        is OutputExp -> visitOutputExp(exp)
    }

    fun <A> visitWithScoped(sc: Scoped<A>): Scoped<A> {
        return Scoped.make(visit(sc.unscope))
    }

    open fun <A> visitV(exp: V<A>): Exp<A> {
        return V(exp.a)
    }

    open fun <A> visitApplication(exp: Application<A>): Exp<A> {
        return Application(visit(exp.f), visit(exp.x))
    }

    open fun <A> visitLambda(exp: Lambda<A>): Exp<A> {
        return Lambda(exp.v, visitWithScoped(exp.f))
    }

    open fun <A> visitLet(exp: Let<A>): Exp<A> {
        return Let(exp.binds.map { visitBind(it) }, visitWithScoped(exp.exp))
    }

    open fun <A> visitBind(bind: Bind<A>): Bind<A> {
        return Bind(bind.name, visitWithScoped(bind.exp), bind.type, bind.pragma)
    }

    open fun <A> visitInjection(exp: Injection<A, Any>): Exp<A> {
        return exp
    }

    open fun <A> visitCase(exp: Case<A>): Exp<A> {
        return Case(visit(exp.exp), exp.alts.map { visitAlt(it) })
    }

    open fun <A> visitAlt(alt: Alt<A>): Alt<A> {
        return Alt(alt.pattern, visitWithScoped(alt.exp))
    }

    open fun <A> visitLangBuiltin(exp: LangBuiltin<A>): Exp<A> {
        return LangBuiltin(exp.builtin)
    }

    open fun <A> visitSet(exp: SetExp<A>): Exp<A> {
        return SetExp(exp.content.map { visit(it) })
    }

    open fun <A> visitHerbrandExp(exp: HerbrandExp<A>): Exp<A> {
        return HerbrandExp(exp.name, exp.args.map { visit(it) })
    }

    open fun <A> visitOutputExp(exp: OutputExp<A>): Exp<A> {
        return OutputExp(exp.name, visit(exp.actualExp))
    }
}
