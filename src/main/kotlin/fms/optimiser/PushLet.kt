package fms.optimiser

import fms.fml.abstract.BoundVar
import fms.fml.abstract.FreeVar
import fms.fml.abstract.Var
import fms.fml.expression.Application
import fms.fml.expression.Bind
import fms.fml.expression.Case
import fms.fml.expression.Exp
import fms.fml.expression.HerbrandExp
import fms.fml.expression.Injection
import fms.fml.expression.Lambda
import fms.fml.expression.LangBuiltin
import fms.fml.expression.Let
import fms.fml.expression.OutputExp
import fms.fml.expression.ScopedExp
import fms.fml.expression.SetExp
import fms.fml.expression.V
import fms.fml.expression.VarT
import fms.fml.expression.emptyInstantiate
import fms.fml.expression.visitor.ExpVisitor

object PushLet : ExpressionTransformer() {
    override fun <A> visitLet(exp: Let<A>): Exp<A> {
        val rec = visitWithScoped(exp.exp).unscope
        val newBinds = exp.binds.map { it.mapExp(PushLet::visitWithScoped) }
        return PushBinds(newBinds).visit(rec)
    }
}

class PushBinds<A>(val binds: List<Bind<A>>) : ExpVisitor<Var<VarT, A>, Exp<A>> {
    override fun visitV(exp: V<Var<VarT, A>>): Exp<A> {
        return Let(binds, ScopedExp.make(exp))
    }

    override fun visitApplication(exp: Application<Var<VarT, A>>): Exp<A> {
        return when {
            exp.f.freeVars().isEmpty() -> {
                Application(ScopedExp.make(exp.f).emptyInstantiate(), visit(exp.x))
            }
            exp.x.freeVars().isEmpty() -> {
                Application(visit(exp.f), ScopedExp.make(exp.x).emptyInstantiate())
            }
            else -> {
                Let(binds, ScopedExp.make(exp))
            }
        }
    }

    override fun visitLambda(exp: Lambda<Var<VarT, A>>): Exp<A> {
        // Swap to layers of bound variables
        val o = exp.f.unscope.liftM { it ->
            val o: Var<VarT, Var<VarT, A>> = when (it) {
                is FreeVar -> {
                    if (it.a is BoundVar) {
                        BoundVar(it.a.b)
                    } else it
                }
                is BoundVar -> FreeVar(BoundVar(it.b))
            }
            o
        }
        val nBinds = binds.map {
            it.mapExp { it.unscope.emptyScope<VarT>() }
        }
        val out = Lambda(exp.v, ScopedExp.make(PushBinds(nBinds).visit(o)))
        return out
    }

    override fun visitLet(exp: Let<Var<VarT, A>>): Exp<A> {
        return Let(binds, ScopedExp.make(exp))
    }

    override fun visitInjection(exp: Injection<Var<VarT, A>, Any>): Exp<A> {
        return ScopedExp.make(exp).emptyInstantiate()
    }

    override fun visitCase(exp: Case<Var<VarT, A>>): Exp<A> {
        // TODO: become smarter
        return Let(binds, ScopedExp.make(exp))
    }

    override fun visitLangBuiltin(exp: LangBuiltin<Var<VarT, A>>): Exp<A> {
        return ScopedExp.make(exp).emptyInstantiate()
    }

    override fun visitSet(exp: SetExp<Var<VarT, A>>): Exp<A> {
        // TODO: become smarter
        return Let(binds, ScopedExp.make(exp))
    }

    override fun visitHerbrandExp(exp: HerbrandExp<Var<VarT, A>>): Exp<A> {
        // TODO: become smarter
        return Let(binds, ScopedExp.make(exp))
    }

    override fun visitOutputExp(exp: OutputExp<Var<VarT, A>>): Exp<A> {
        return OutputExp(exp.name, visit(exp.actualExp))
    }
}
