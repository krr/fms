package fms.optimiser

import fms.fml.expression.Expression
import fms.optimiser.rules.RecursiveApply
import fms.optimiser.rules.boolRules

data class OptiSettings(
    var tarjanLet: Boolean,
    var pushNegs: Boolean,
    var inlineSingleUse: Boolean,
    var pushLet: Boolean,
    var constantFolding: Boolean
) {

    companion object {
        val all: OptiSettings
            get() = OptiSettings(true, true, true, true, true)
        val none: OptiSettings
            get() = OptiSettings(true, false, false, false, false)
    }
}

fun optimise(exp: Expression, set: OptiSettings): Expression {
    var curExp = exp

    if (set.tarjanLet) {
        curExp = TarjanLet.visit(curExp)
    }
    // TODO find fixpoint?
    for (i in 1..50) {
        if (set.pushLet) {
            curExp = PushLet.visit(curExp)
        }
        if (set.inlineSingleUse) {
            curExp = InlineLet.visit(curExp)
        }
        if (set.pushNegs) {
            curExp = RecursiveApply(boolRules).visit(curExp)
        }

        if (set.constantFolding) {
            curExp = ConstantFolder.visit(curExp)
        }
    }
    return curExp
}
