package fms.optimiser.rules

import fms.fml.expression.Exp

typealias Hole = String

data class Rule(
    val inPattern: Exp<Hole>,
    val outPattern: Exp<Hole>
) {
    fun <A> applyLocally(exp: Exp<A>): Exp<A>? {
        val m = Match(exp).visit(inPattern) ?: return null
        return outPattern.bind { m[it]!! }
    }
}

data class RuleSet(
    val name: String,
    val rules: Set<Rule>
) {
    fun <A> applyLocally(rec: Exp<A>): Exp<A>? {
        var curExp = rec
        var changed = true
        while (changed) {
            changed = false
            rules.forEach {
                val out = it.applyLocally(curExp)
                if (out != null) {
                    changed = true
                    curExp = out
                }
            }
        }
        if (curExp === rec) {
            return null
        }
        return curExp
    }
}