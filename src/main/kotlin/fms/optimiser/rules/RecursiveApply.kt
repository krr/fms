package fms.optimiser.rules

import fms.fml.expression.Exp
import fms.optimiser.ExpressionTransformer

class RecursiveApply(val input: RuleSet) : ExpressionTransformer() {
    override tailrec fun <A> visit(exp: Exp<A>): Exp<A> {
        val rec = super.visit(exp)
        val r = input.applyLocally(rec) ?: return rec
        return visit(r)
    }
}