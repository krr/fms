package fms.optimiser.rules

import fms.parsers.asExpr

val notnot = Rule(asExpr("~~a"), asExpr("a"))
val nand = Rule(asExpr("~(a & b)"), asExpr("~a | ~b"))
val nor = Rule(asExpr("~(a | b)"), asExpr("~a & ~b"))
val neq = Rule(asExpr("~(a = b)"), asExpr("a ~= b"))
val nneq = Rule(asExpr("~(a ~= b)"), asExpr("a = b"))
val ngt = Rule(asExpr("~(a > b)"), asExpr("a =< b"))
val nlt = Rule(asExpr("~(a < b)"), asExpr("a >= b"))
val nleq = Rule(asExpr("~(a =< b)"), asExpr("a > b"))
val ngeq = Rule(asExpr("~(a >= b)"), asExpr("a < b"))
val forall = Rule(asExpr("! s f"), asExpr("~(? s (\\x -> ~ (f x)))"))

val boolRules = RuleSet("Boolean Rules", setOf(notnot, nand, nor, neq, nneq, ngt, nlt, nleq, ngeq, forall))