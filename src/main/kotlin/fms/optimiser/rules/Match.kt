package fms.optimiser.rules

import fms.fml.expression.Application
import fms.fml.expression.Case
import fms.fml.expression.Exp
import fms.fml.expression.HerbrandExp
import fms.fml.expression.Injection
import fms.fml.expression.Lambda
import fms.fml.expression.LangBuiltin
import fms.fml.expression.Let
import fms.fml.expression.OutputExp
import fms.fml.expression.SetExp
import fms.fml.expression.V
import fms.fml.expression.visitor.ExpVisitor
import fms.fml.type.union

class Match<A>(val input: Exp<A>) : ExpVisitor<Hole, Map<Hole, Exp<A>>?> {
    override fun visitV(exp: V<Hole>): Map<Hole, Exp<A>>? {
        return mapOf(exp.a to input)
    }

    override fun visitApplication(exp: Application<Hole>): Map<Hole, Exp<A>>? {
        if (input is Application) {
            val f = Match(input.f).visit(exp.f) ?: return null
            val x = Match(input.x).visit(exp.x) ?: return null
            return f.union(x)
        }
        return null
    }

    override fun visitLambda(exp: Lambda<Hole>): Map<Hole, Exp<A>>? {
        return null
    }

    override fun visitLet(exp: Let<Hole>): Map<Hole, Exp<A>>? {
        return null
    }

    override fun visitInjection(exp: Injection<Hole, Any>): Map<Hole, Exp<A>>? {
        if (exp == input) {
            return mapOf()
        }
        return null
    }

    override fun visitCase(exp: Case<Hole>): Map<Hole, Exp<A>>? {
        // TODO: Finish this
        return null
    }

    override fun visitLangBuiltin(exp: LangBuiltin<Hole>): Map<Hole, Exp<A>>? {
        if (exp == input) {
            return mapOf()
        }
        return null
    }

    override fun visitSet(exp: SetExp<Hole>): Map<Hole, Exp<A>>? {
        if (input is SetExp && exp.content.size == input.content.size) {
            val maps = exp.content.zip(input.content) { a, b -> Match(b).visit(a) ?: return null }
            return maps.fold(mapOf()) { a, b -> a.union(b) }
        }
        return null
    }

    override fun visitHerbrandExp(exp: HerbrandExp<Hole>): Map<Hole, Exp<A>>? {
        if (input is HerbrandExp && exp.name == input.name && exp.args.size == input.args.size) {
            val maps = exp.args.zip(input.args) { a, b -> Match(b).visit(a) ?: return null }
            return maps.fold(mapOf()) { a, b -> a.union(b) }
        }
        return null
    }

    override fun visitOutputExp(exp: OutputExp<Hole>): Map<Hole, Exp<A>>? {
        return null
    }
}