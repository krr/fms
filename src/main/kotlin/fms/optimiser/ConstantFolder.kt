package fms.optimiser

import fms.asp.ArithOp
import fms.fml.expression.Application
import fms.fml.expression.Bind
import fms.fml.expression.BindPragma
import fms.fml.expression.Case
import fms.fml.expression.Exp
import fms.fml.expression.Injection
import fms.fml.expression.LangBuiltin
import fms.fml.expression.Let
import fms.fml.expression.emptyInstantiate

object ConstantFolder : ExpressionTransformer() {
    override fun <A> visitApplication(exp: Application<A>): Exp<A> {
        val (f, xs) = exp.collectArgs()
        if (f is LangBuiltin && f.builtin is ArithOp && xs.size == 2) {
            val args = xs.map { visit(it) }
            if (args.all { it is Injection<*, *> }) {
                return Injection(f.builtin.calc(args.map { (it as Injection<*, *>).value as Int }))
            }
        }
        return super.visitApplication(exp)
    }

    override fun <A> visitCase(exp: Case<A>): Exp<A> {
        val vexp = visit(exp.exp)
        for ((pat, e) in exp.alts) {
            val matchMap = pat.match(vexp)
            if (matchMap != null) {
                val binds = pat.varList().map { Bind(it, matchMap[it]!!.emptyScope(), null, BindPragma()) }
                return visit(Let(binds, e))
            }
            if (pat.canMatch(vexp)) {
                return super.visitCase(exp)
            }
        }
        return super.visitCase(exp)
    }

    override fun <A> visitLet(exp: Let<A>): Exp<A> {
        if (exp.binds.isEmpty()) {
            return exp.exp.emptyInstantiate()
        }
        return super.visitLet(exp)
    }
}