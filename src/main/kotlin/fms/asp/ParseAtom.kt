package fms.asp

import fms.parsers.AtomLexer
import fms.parsers.AtomParser
import fms.parsers.AtomParserBaseVisitor
import org.antlr.v4.runtime.CharStreams
import org.antlr.v4.runtime.CommonTokenStream
import java.io.InputStream

fun lexStream(stream: InputStream) =
        AtomLexer(CharStreams.fromStream(stream))

fun parseStream(stream: InputStream): AtomParser =
        AtomParser(CommonTokenStream(lexStream(stream)))

fun lexString(string: String) =
        AtomLexer(CharStreams.fromString(string))

fun parseString(string: String) =
        AtomParser(CommonTokenStream(lexString(string)))

fun asAtom(string: String) =
        ParseAtom.visit(parseString(string).atom())

object ParseArg : AtomParserBaseVisitor<Domain>() {

    override fun visitIntLit(ctx: AtomParser.IntLitContext): Domain {
        val int = Integer.parseInt(ctx.INTLIT().text)
        return DInt(int)
    }

    override fun visitStringLit(ctx: AtomParser.StringLitContext): Domain {
        return DString(ctx.STRING().text.removeSurrounding("\""))
    }

    override fun visitHerbrand(ctx: AtomParser.HerbrandContext): Domain {
        return DHerbrand(ctx.ID().text, ctx.arg().map { visit(it) })
    }

    override fun visitTuple(ctx: AtomParser.TupleContext): Domain {
        return DTuple(ctx.arg().map { visit(it) })
    }
}

object ParseAtom : AtomParserBaseVisitor<Pair<String, DHerbrand>>() {
    override fun visitAtom(ctx: AtomParser.AtomContext): Pair<String, DHerbrand> {
        return ctx.ID().text to DTuple(ctx.arg().map { ParseArg.visit(it) })
    }
}