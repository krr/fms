package fms.asp

import java.util.SortedSet

sealed class Domain : Comparable<Domain> {
    override fun compareTo(other: Domain): Int {
        if (this is DInt && other is DInt) {
            return this.s.compareTo(other.s)
        } else if (this is DString && other is DString) {
            return this.s.compareTo(other.s)
        } else if (this is DHerbrand && other is DHerbrand) {
            if (this.name != other.name) {
                return this.name.compareTo(other.name)
            } else {
                for ((i, j) in args.zip(other.args)) {
                    if (i != j) {
                        return i.compareTo(j)
                    }
                }
                return 0
            }
        } else if (this is DSet && other is DSet) {
            return DTuple(this.s.toList()).compareTo(DTuple(other.s.toList()))
        }
        return this.javaClass.canonicalName.compareTo(other.javaClass.canonicalName)
    }
}

data class DInt(val s: Int) : Domain() {
    override fun toString(): String {
        return s.toString()
    }
}

data class DString(val s: String) : Domain() {
    override fun toString(): String {
        return s
    }
}

fun DTuple(args: List<Domain>) = DHerbrand("", args)

data class DHerbrand(val name: String, val args: List<Domain>) : Domain() {

    override fun toString(): String {
        if (name == "" && args.isEmpty()) {
            return "()"
        }
        if (name == "" && args.size == 1) {
            return "${args[0]}"
        }
        if (args.isEmpty()) {
            return name
        }
        return "$name${args.joinToString(prefix = "(", postfix = ")", separator = ",")}"
    }
}

data class DSet(val s: SortedSet<Domain>) : Domain() {
    companion object {
        val empty = DSet(sortedSetOf())
    }
    override fun toString(): String {
        return s.toString()
    }
}