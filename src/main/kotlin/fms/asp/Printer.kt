import fms.asp.Abs
import fms.asp.AggBody
import fms.asp.AggElement
import fms.asp.Aggregate
import fms.asp.AggregateFunction
import fms.asp.AggregateLit
import fms.asp.AnonymVar
import fms.asp.ArithOp
import fms.asp.Arithm
import fms.asp.Binop
import fms.asp.Body
import fms.asp.BuiltinNaf
import fms.asp.ByString
import fms.asp.ChoiceElement
import fms.asp.ChoiceHead
import fms.asp.ClassLit
import fms.asp.ClassNaf
import fms.asp.Constraint
import fms.asp.DisjunctiveHead
import fms.asp.Head
import fms.asp.Herbrand
import fms.asp.IntTerm
import fms.asp.LitBody
import fms.asp.NafLit
import fms.asp.Optimize
import fms.asp.OptimizeFunction
import fms.asp.Program
import fms.asp.RangeTerm
import fms.asp.Rule
import fms.asp.Sign
import fms.asp.Statement
import fms.asp.StatementVisitor
import fms.asp.StringTerm
import fms.asp.Term
import fms.asp.TermVisitor
import fms.asp.UnaryMinus
import fms.asp.Variable
import fms.asp.WeakConstraint
import java.io.PrintWriter

val defines = ":-"
val dot = "."
val weak = ":~"

fun <T> intersperseWith(sep: String, action: T.(PrintWriter) -> Unit, els: Iterable<T>, ps: PrintWriter) {
    els.firstOrNull()?.action(ps) ?: return
    els.drop(1).forEach {
        ps.print(sep)
        it.action(ps)
    }
}

fun Program.toStream(ps: PrintWriter) {
    this.queries.forEach { it.toStream(ps); ps.println() }
    this.statements.forEach { it.toStream(ps); ps.println() }
    this.objective?.toStream(ps); ps.println()
}

class StatementStreamer(val ps: PrintWriter) : StatementVisitor<Unit>() {
    override fun visitConstraint(c: Constraint) {
        ps.print(defines)
        intersperseWith(",", Body::toStream, c.constraint, ps)
        ps.print(dot)
    }

    override fun visitRule(r: Rule) {
        r.head.toStream(ps)
        if (r.body.isNotEmpty()) {
            ps.print(defines)
            intersperseWith(",", Body::toStream, r.body.toSet(), ps)
        }
        ps.print(dot)
    }

    override fun visitOptimize(o: Optimize) {
        o.optimFunction.toStream(ps)
        ps.print("{")
        intersperseWith(";", AggElement::toStream, o.aggElem, ps)
        ps.print("}")
        ps.print(dot)
    }

    override fun visitByString(b: ByString) {
        ps.print(b.str)
    }

    override fun visitWeakConstraint(w: WeakConstraint) {
        ps.print(weak)
        if (w.body.isNotEmpty()) {
            intersperseWith(",", Body::toStream, w.body, ps)
        }
        ps.print(dot)
        ps.print(" [")
        if (w.weights.isNotEmpty()) {
            intersperseWith(",", Term::toStream, w.weights, ps)
        }
        ps.print("]")
    }
}

fun Statement.toStream(ps: PrintWriter) {
    StatementStreamer(ps).visit(this)
}

fun Head.toStream(ps: PrintWriter) {
    when (this) {
        is DisjunctiveHead -> intersperseWith(";", ClassLit::toStream, lits, ps)
        is ChoiceHead -> {
            if (underBound != null && underBound != upperBound) {
                ps.print(underBound)
                ps.print("<=")
            }
            ps.print("{")
            intersperseWith(";", ChoiceElement::toStream, choiceEls, ps)
            ps.print("}")
            if (upperBound != null && underBound != upperBound) {
                ps.print("<=")
                ps.print(upperBound)
            }
            if (upperBound != null && underBound == upperBound) {
                ps.print("==")
                ps.print(underBound)
            }
        }
    }
}

fun ChoiceElement.toStream(ps: PrintWriter) {
    defined.toStream(ps)
    ps.print(":")
    intersperseWith(",", Body::toStream, generator, ps)
}

fun Body.toStream(ps: PrintWriter) {
    when (this) {
        is LitBody -> naflit.toStream(ps)
        is AggBody -> {
            if (isNaf) ps.print("not ")
            aggLit.toStream(ps)
        }
    }
}

fun AggregateLit.toStream(ps: PrintWriter) {
    aggregate.toStream(ps)
    binop.toStream(ps)
    term.toStream(ps)
}

fun NafLit.toStream(ps: PrintWriter) {
    when (this) {
        is ClassNaf -> {
            if (sign == Sign.Negative) {
                ps.print("not ")
            }
            lit.toStream(ps)
        }
        is BuiltinNaf -> {
            operand1.toStream(ps)
            binop.toStream(ps)
            operand2.toStream(ps)
        }
    }
}

fun Binop.toStream(ps: PrintWriter) {
    when (this) {
        Binop.EQ -> ps.print("=")
        Binop.NEQ -> ps.print("<>")
        Binop.LT -> ps.print("<")
        Binop.GT -> ps.print(">")
        Binop.LEQ -> ps.print("<=")
        Binop.GEQ -> ps.print(">=")
    }
}

class TermStreamer(val ps: PrintWriter) : TermVisitor<Unit>() {
    override fun visitHerbrand(h: Herbrand) {
        if (h.name == "" && h.args.isEmpty()) {
            ps.print("()")
        } else if (h.args.isEmpty() && h.name.first() != '@') {
            ps.print(h.name)
        } else {
            ps.print(h.name)
            ps.print("(")
            intersperseWith(",", Term::toStream, h.args, ps)
            ps.print(")")
        }
    }

    override fun visitIntTerm(i: IntTerm) {
        ps.print(i.int)
    }

    override fun visitStringTerm(st: StringTerm) {
        ps.print("\"" + st.string + "\"")
    }

    override fun visitVariable(v: Variable) {
        ps.print(v.name)
    }

    override fun visitAnonymVar(a: AnonymVar) {
        ps.print("_")
    }

    override fun visitUnaryMinus(u: UnaryMinus) {
        ps.print("-")
        u.term.toStream(ps)
    }

    override fun visitAbs(a: Abs) {
        ps.print("|")
        a.term.toStream(ps)
        ps.print("|")
    }

    override fun visitArithm(a: Arithm) {
        ps.print("(")
        a.operand1.toStream(ps)
        a.operator.toStream(ps)
        a.operand2.toStream(ps)
        ps.print(")")
    }

    override fun visitRangeTerm(r: RangeTerm) {
        r.underBound.toStream(ps)
        ps.print("..")
        r.upperBound.toStream(ps)
    }
}

fun Term.toStream(ps: PrintWriter) {
    TermStreamer(ps).visit(this)
}

fun ArithOp.toStream(ps: PrintWriter) {
    when (this) {
        ArithOp.ArPlus -> ps.print("+")
        ArithOp.ArMinus -> ps.print("-")
        ArithOp.ArTimes -> ps.print("*")
        ArithOp.ArDiv -> ps.print("/")
        ArithOp.ArMod -> ps.print("\\")
        ArithOp.ArPow -> ps.print("**")
    }
}

fun ClassLit.toStream(ps: PrintWriter) {
    if (sign == Sign.Negative) {
        ps.print("not ")
    }
    ps.print(predName)
    if (args.isNotEmpty()) {
        ps.print("(")
        intersperseWith(",", Term::toStream, args, ps)
        ps.print(")")
    }
}

fun Aggregate.toStream(ps: PrintWriter) {
    aggFunction.toStream(ps)
    ps.print("{")
    intersperseWith(";", AggElement::toStream, aggElem, ps)
    ps.print("}")
}

fun AggregateFunction.toStream(ps: PrintWriter) {
    when (this) {
        AggregateFunction.AggCount -> ps.print("#count")
        AggregateFunction.AggMax -> ps.print("#max")
        AggregateFunction.AggMin -> ps.print("#min")
        AggregateFunction.AggSum -> ps.print("#sum")
    }
}

fun AggElement.toStream(ps: PrintWriter) {
    intersperseWith(",", Term::toStream, terms, ps)
    ps.print(":")
    intersperseWith(",", Body::toStream, nafs.toSet(), ps)
}

fun OptimizeFunction.toStream(ps: PrintWriter) {
    when (this) {
        OptimizeFunction.Minimize -> ps.print("#minimize")
        OptimizeFunction.Maximize -> ps.print("#maximize")
    }
}
