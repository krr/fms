package fms.asp

abstract class StatementVisitor<A> {
    abstract fun visitConstraint(c: Constraint): A
    abstract fun visitRule(r: Rule): A
    abstract fun visitOptimize(o: Optimize): A
    abstract fun visitByString(b: ByString): A
    abstract fun visitWeakConstraint(w: WeakConstraint): A

    fun visit(s: Statement): A {
        return when (s) {
            is Constraint -> visitConstraint(s)
            is Rule -> visitRule(s)
            is Optimize -> visitOptimize(s)
            is ByString -> visitByString(s)
            is WeakConstraint -> visitWeakConstraint(s)
        }
    }
}

abstract class TermVisitor<A> {
    abstract fun visitHerbrand(h: Herbrand): A
    abstract fun visitIntTerm(i: IntTerm): A
    abstract fun visitStringTerm(st: StringTerm): A
    abstract fun visitVariable(v: Variable): A
    abstract fun visitAnonymVar(a: AnonymVar): A
    abstract fun visitUnaryMinus(u: UnaryMinus): A
    abstract fun visitAbs(a: Abs): A
    abstract fun visitArithm(a: Arithm): A
    abstract fun visitRangeTerm(r: RangeTerm): A

    fun visit(s: Term): A {
        return when (s) {
            is Herbrand -> visitHerbrand(s)
            is IntTerm -> visitIntTerm(s)
            is StringTerm -> visitStringTerm(s)
            is Variable -> visitVariable(s)
            is AnonymVar -> visitAnonymVar(s)
            is UnaryMinus -> visitUnaryMinus(s)
            is Abs -> visitAbs(s)
            is Arithm -> visitArithm(s)
            is RangeTerm -> visitRangeTerm(s)
        }
    }
}