package fms.asp

import toStream
import java.io.File
import java.io.PrintWriter

fun Program.toTempFile(): File {
    val f = File.createTempFile("fml", "asp")
    val stream = PrintWriter(f)
    this.toStream(stream)
    stream.close()
    return f
}

fun runClingo(f: File, nbModels: Int, stats: Boolean): Process {
    val pb = ProcessBuilder("clingo", "--outf=2", nbModels.toString(), if (stats) "-s1" else "-s0", f.absolutePath)
    val process = pb.start()
    return process
}
