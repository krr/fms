package fms.asp

import fms.fml.builtin.BoolBuiltin
import fms.fml.builtin.Builtin
import fms.fml.expression.Exp
import fms.parsers.asScheme
import fms.translation.SafeVariable
import fms.translation.Translator

data class Program(val statements: List<Statement>, val queries: List<ClassLit>, val objective: Optimize? = null)

sealed class Statement
data class Constraint(val constraint: List<Body>) : Statement()
data class Rule(val head: Head, val body: List<Body>) : Statement()
data class Optimize(val optimFunction: OptimizeFunction, val aggElem: List<AggElement>) : Statement()
data class ByString(val str: String) : Statement()
data class WeakConstraint(val body: List<Body>, val weights: List<Term>) : Statement()
// class Weight //TODO
// class Priority //TODO -- optional
enum class OptimizeFunction { Maximize, Minimize }

sealed class Head {
    companion object {
        fun literal(predName: String, args: List<Term> = listOf()): DisjunctiveHead =
                DisjunctiveHead(listOf(ClassLit(Sign.Positive, predName, args)))
    }
}

data class DisjunctiveHead(val lits: List<ClassLit>) : Head()
data class ChoiceHead(val choiceEls: List<ChoiceElement>, val underBound: Int? = null, val upperBound: Int? = null) : Head()

data class ChoiceElement(val defined: ClassLit, val generator: List<Body>)

enum class Binop : BoolBuiltin {
    EQ, NEQ, LT, GT, LEQ, GEQ;

    fun negate(): Binop = when (this) {
        EQ -> NEQ
        NEQ -> EQ
        LT -> GEQ
        GT -> LEQ
        LEQ -> GT
        GEQ -> LT
    }

    override val type = asScheme("a -> a -> Bool")

    override fun translateBool(args: List<Exp<SafeVariable>>, tr: Translator): SafeVariable {
        assert(args.size == 2)
        val arg1 = tr.visit(args[0])
        val arg2 = tr.visit(args[1])
        val lit = LitBody(BuiltinNaf(arg1.term, this, arg2.term))
        return SafeVariable(Term.tuple(), setOf(lit) + arg1.safety + arg2.safety)
    }
}

sealed class Term {
    companion object {
        fun tuple(vararg els: Term): Herbrand = Herbrand("", els.toList())
        fun tuple(els: Iterable<Term>): Herbrand = Herbrand("", els.toList())
    }

    abstract fun freeVars(): List<Variable>
}

data class Herbrand(val name: String, val args: List<Term> = listOf()) : Term() {
    override fun freeVars(): List<Variable> {
        return args.flatMap { it.freeVars() }
    }
}

data class IntTerm(val int: Int) : Term() {
    override fun freeVars(): List<Variable> {
        return listOf()
    }
}

data class StringTerm(val string: String) : Term() {
    override fun freeVars(): List<Variable> {
        return listOf()
    }
}

data class Variable(val name: String) : Term() {
    override fun freeVars(): List<Variable> {
        return listOf(this)
    }
}

data class UnaryMinus(val term: Term) : Term() {
    override fun freeVars(): List<Variable> {
        return term.freeVars()
    }
}

data class Abs(val term: Term) : Term() {
    override fun freeVars(): List<Variable> {
        return term.freeVars()
    }
}

data class Arithm(val operand1: Term, val operator: ArithOp, val operand2: Term) : Term() {
    override fun freeVars(): List<Variable> {
        return operand1.freeVars() + operand2.freeVars()
    }
}

data class RangeTerm(val underBound: Term, val upperBound: Term) : Term() {
    override fun freeVars(): List<Variable> {
        return underBound.freeVars() + upperBound.freeVars()
    }
}

object AnonymVar : Term() {
    override fun freeVars(): List<Variable> {
        return listOf()
    }
}

enum class ArithOp : Builtin {
    ArPlus, ArMinus, ArTimes, ArDiv, ArMod, ArPow;

    override val type = asScheme("Int -> Int -> Int")

    override fun translate(args: List<Exp<SafeVariable>>, tr: Translator): SafeVariable {
        assert(args.size == 2)
        val arg1 = tr.visit(args[0])
        val arg2 = tr.visit(args[1])
        return arg1.combine(arg2) { t1, t2 -> Arithm(t1, this, t2) }
    }

    fun calc(args: List<Int>): Int {
        assert(args.size == 2)
        return when (this) {
            ArPlus -> args[0] + args[1]
            ArMinus -> args[0] - args[1]
            ArTimes -> args[0] * args[1]
            ArDiv -> args[0] / args[1]
            ArMod -> args[0] % args[1]
            ArPow -> args[0].toBigInteger().pow(args[1]).toInt()
        }
    }
}

enum class Sign {
    Positive, Negative;

    fun negate(): Sign = when (this) {
        Positive -> Negative
        Negative -> Positive
    }
}

sealed class NafLit {
    abstract fun negate(): NafLit
    abstract fun freeVars(): Iterable<Variable>
}

data class ClassNaf(val sign: Sign, val lit: ClassLit) : NafLit() {
    override fun freeVars(): Iterable<Variable> {
        return lit.freeVars()
    }

    override fun negate(): NafLit = ClassNaf(sign, lit.negate())
}

data class BuiltinNaf(val operand1: Term, val binop: Binop, val operand2: Term) : NafLit() {
    override fun freeVars(): Iterable<Variable> {
        return operand1.freeVars() + operand2.freeVars()
    }

    override fun negate(): NafLit = BuiltinNaf(operand1, binop.negate(), operand2)
}

data class AggregateLit(val aggregate: Aggregate, val binop: Binop, val term: Term) {
    fun freeVars(): Iterable<Variable> {
        return term.freeVars() + aggregate.freeVars()
    }
}

data class Aggregate(val aggFunction: AggregateFunction, val aggElem: List<AggElement>) {
    fun freeVars(): Iterable<Variable> {
        return aggElem.flatMap { it.freeVars() }
    }
}

enum class AggregateFunction {
    AggCount, AggMax, AggMin, AggSum
}

data class AggElement(val terms: List<Term>, val nafs: List<Body>) {
    fun freeVars(): Iterable<Variable> {
        return terms.flatMap { it.freeVars() } + nafs.flatMap { it.freeVars() }
    }
}

sealed class Body {
    companion object {
        fun literal(predName: String, args: List<Term> = listOf()): LitBody =
                LitBody(ClassNaf(Sign.Positive, ClassLit(Sign.Positive, predName, args)))

        fun literal(sign: Sign, predName: String, args: List<Term> = listOf()): LitBody =
                LitBody(ClassNaf(Sign.Positive, ClassLit(sign, predName, args)))
    }

    abstract fun negate(): Body
    abstract fun freeVars(): Iterable<Variable>
}

data class LitBody(val naflit: NafLit) : Body() {
    override fun freeVars(): Iterable<Variable> {
        return naflit.freeVars()
    }

    override fun negate(): Body = LitBody(naflit.negate())
}

data class AggBody(val isNaf: Boolean, val aggLit: AggregateLit) : Body() {
    override fun freeVars(): Iterable<Variable> {
        return aggLit.freeVars()
    }

    override fun negate(): Body = AggBody(!isNaf, aggLit)
}

data class ClassLit(val sign: Sign, val predName: String, val args: List<Term>) {
    fun negate(): ClassLit = ClassLit(sign.negate(), predName, args)
    fun freeVars(): Iterable<Variable> {
        return args.flatMap { it.freeVars() }
    }
}
