package fms.asp

import com.beust.klaxon.JsonArray
import com.beust.klaxon.JsonObject
import com.beust.klaxon.Parser
import fms.fml.type.Type
import java.io.InputStream
import java.util.SortedMap
import java.util.SortedSet
import java.util.TreeMap

data class ClingoResult(
    val solver: String,
    val witnesses: List<Model>,
    val result: String,
    val moreModels: Boolean,
    val calls: Int,
    val timing: Map<String, Double>,
    var exitCode: Int = 0,
    var errorStream: String = ""
) {

    fun outputString(): String {
        val out = StringBuilder()
        out.appendln("${witnesses.size} models:")
        witnesses.map { out.appendln(it.toString()) }
        if (moreModels) {
            out.appendln("(possibly more)")
        }
        out.appendln()
        out.appendln(timing.toString())
        out.appendln("Exit Code: $exitCode")
        return out.toString()
    }

    companion object {

        @Suppress("UNCHECKED_CAST")
        fun fromJSON(str: InputStream): ClingoResult {
            val r = Parser().parse(str) as JsonObject

            val solver = r["Solver"] as String
            val call = r["Call"] as JsonArray<JsonObject>
            assert(call.size == 1)
            val witnessessO = call[0]["Witnesses"] as JsonArray<JsonObject>?
            val witnesses = witnessessO?.map { Model.make((it["Value"] as JsonArray<String>).toList()) } ?: listOf()
            val result = r["Result"] as String
            val moreModels = (r["Models"] as JsonObject)["More"] as String == "yes"
            val calls = r["Calls"] as Int
            val timing = (r["Time"] as JsonObject).map.mapValues { it.value as Double }
            val more = if (r.containsKey("Stats")) {
                (r["Stats"] as JsonObject).toJsonString(true)
            } else {
                ""
            }
            return ClingoResult(solver, witnesses, result, moreModels, calls, timing, errorStream = more)
        }
    }

    fun unMeta(t: Map<String, Type>): ClingoResult {
        return this.copy(witnesses = witnesses.map { unMeta(it, t) })
    }
}

data class Model(val atoms: SortedMap<String, Domain>) {

    companion object {
        fun make(atoms: List<String>): Model {
            val splits: List<Pair<String, Domain>> = atoms.map(::asAtom)
            val out = TreeMap<String, SortedSet<Domain>>()
            splits.forEach { (name, args) ->
                out.merge(name, sortedSetOf(args)) { list, elements -> list.apply { addAll(elements) } }
            }
            return Model(out.mapValues { DSet(it.value) }.toSortedMap())
        }
    }

    operator fun get(s: String): Domain? {
        return this.atoms[s]
    }

    override fun toString(): String {
        return atoms.toString()
    }
}
