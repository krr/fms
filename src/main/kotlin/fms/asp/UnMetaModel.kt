package fms.asp

import fms.globalLog
import fms.fml.type.Apply
import fms.fml.type.BaseType
import fms.fml.type.Const
import fms.fml.type.Type
import fms.parsers.asType
import java.util.TreeMap

fun unMeta(m: Model, t: Map<String, Type>): Model {
    val out = TreeMap<String, Domain>()
    val c = (m["out"] as? DSet ?: DSet.empty).s
    c.forEach {
        it as DHerbrand
        val name = (it.args[0] as? DString)?.s
                ?: throw Error("Out should always have strings in first argument position")
        val type = t[name] ?: asType("a")
        val content = it.args[1]
        val contentInter = unMetaDomain(m, content, type)
        out[name] = contentInter
        return@forEach
    }
    return Model(out)
}

fun unMetaDomain(m: Model, domain: Domain, type: Type): Domain {
    when {
        type.canonicEqual(asType("a")) -> return domain
        type.canonicEqual(asType("Bool")) -> return unMetaBool(m, domain)
        type is Apply -> {
            val f = type.t1
            when {
                type.leftMost.let { it is Const && it.t1 == BaseType.Tuple } -> {
                    if (domain is DHerbrand) {
                        return unMetaTuple(m, domain.args, type)
                    }
                    globalLog.trace { "Tuples should be DHerbrand! Current is ${domain.javaClass}" }
                }
                f is Apply -> {
                    val g = f.t1
                    if (g is Const && g.t1 == BaseType.Arrow) {
                        return unMetaFunc(m, domain, f.t2, type.t2)
                    }
                }
                f is Const && f.t1 == BaseType.Set -> {
                    return unMetaSet(m, domain, type.t2)
                }
            }
        }
    }
    globalLog.trace { "Unknown type to unMeta: $type" }
    return domain
}

fun unMetaTuple(m: Model, args: List<Domain>, type: Type): DHerbrand {
    return when {
        args.isEmpty() -> DHerbrand("", emptyList())
        type !is Apply -> throw Error()
        else -> {
            val last = unMetaDomain(m, args.last(), type.t2)
            val rest = unMetaTuple(m, args.dropLast(1), type.t1)
            DHerbrand("", rest.args + last)
        }
    }
}

fun unMetaSet(m: Model, domain: Domain, t: Type): Domain {
    val out = (m["member"] as? DSet ?: DSet.empty).s.mapNotNull {
        it as DHerbrand
        if (it.args[0] == domain) {
            unMetaDomain(m, it.args[1], t)
        } else {
            null
        }
    }
    return DSet(out.toSortedSet())
}

fun unMetaFunc(m: Model, domain: Domain, t1: Type, t2: Type): Domain {
    val out = (m["lamInter"] as? DSet ?: DSet.empty).s.mapNotNull {
        it as DHerbrand
        if (it.args[0] == domain) {
            val a = unMetaDomain(m, it.args[1], t1)
            val b = unMetaDomain(m, it.args[2], t2)
            DTuple(listOf(a, b))
        } else {
            null
        }
    }
    return DSet(out.toSortedSet())
}

fun unMetaBool(m: Model, domain: Domain): Domain {
    return if ((m["bool"] as? DSet ?: DSet.empty).s.contains(DTuple(listOf(domain, DTuple(listOf()))))) {
        DHerbrand("true", listOf())
    } else {
        DHerbrand("false", listOf())
    }
}