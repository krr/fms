package fms.fml.builtin

import fms.asp.ChoiceElement
import fms.asp.ChoiceHead
import fms.asp.ClassLit
import fms.asp.Rule
import fms.asp.Sign
import fms.asp.Term
import fms.fml.expression.Application
import fms.fml.expression.Exp
import fms.fml.expression.LangBuiltin
import fms.fml.expression.V
import fms.parsers.asScheme
import fms.translation.Environment
import fms.translation.SafeVariable
import fms.translation.Translator

object Extract : Builtin {
    override val type = asScheme("Set a -> a")

    override fun translate(args: List<Exp<SafeVariable>>, tr: Translator): SafeVariable {
        assert(args.size == 1)
        return tr.setTrans().visit(args[0])
    }
}

object Graph : Builtin {
    override val type = asScheme("(a -> b)-> c")

    override fun translate(args: List<Exp<SafeVariable>>, tr: Translator): SafeVariable {
        assert(args.size == 1)
        val lam = tr.visit(args[0])
        val inpVar = tr.e.newVar()
        val inpSafe = tr.e.lambda.eval(lam, inpVar).safety
        val outpVar = tr.e.applyLam(lam, SafeVariable(inpVar, inpSafe))
        return SafeVariable(Term.tuple(inpVar, outpVar.term), lam.safety + inpSafe + outpVar.safety)
    }
}

object SubsetDeclare : Builtin {

    override val type = asScheme("Set a -> Set a")

    override fun translate(args: List<Exp<SafeVariable>>, tr: Translator): SafeVariable {
        assert(args.size == 1)
        val set = tr.visit(args[0])
        return tr.e.subset(set)
    }
}

class ElementDeclare(val partial: Boolean) : Builtin {

    override val type = asScheme("Set a -> a")

    override fun translate(args: List<Exp<SafeVariable>>, tr: Translator): SafeVariable {
        if (args.size > 1) {
            return tr.visit(Application.make(V(translate(args.take(1), tr)), args.drop(1)))
        }
        assert(args.size == 1)
        val set = tr.visit(args[0])
        return tr.e.set.eval(tr.e.subset(set, if (partial) 0 else 1, 1), tr.e.newVar())
    }
}

class FuncDeclare(val partial: Boolean) : Builtin {

    override val type = asScheme("Set a -> (b -> a)")

    override fun translate(args: List<Exp<SafeVariable>>, tr: Translator): SafeVariable {
        if (args.size > 1) {
            val func = tr.visit(Application.make(LangBuiltin(FuncDeclare(partial)), args.take(1)))
            return tr.visit(Application.make(V(func), args.drop(1)))
        }
        assert(args.size == 1)
        val to = tr.e.set.eval(tr.visit(args[0]), tr.e.newVar())
        return tr.e.function(to, partial)
    }
}

object PropDeclare : Builtin {

    override val type = asScheme("Bool")

    override fun translate(args: List<Exp<SafeVariable>>, tr: Translator): SafeVariable {
        assert(args.size == 0)
        val bool = tr.e.scope.varRef(tr.e.bool.new())

        val classLit = ClassLit(Sign.Positive, tr.e.bool.predName, listOf(bool.term, Term.tuple()))
        val choiceHead = ChoiceHead(listOf(ChoiceElement(classLit, listOf())))

        val rule = Rule(choiceHead, tr.e.scope.safety().toList())
        tr.e.aspProg.add(rule)
        return bool
    }
}

object PredDeclare : Builtin {

    override val type = asScheme("a -> Bool")

    override fun translate(args: List<Exp<SafeVariable>>, tr: Translator): SafeVariable {
        if (args.size > 0) {
            return tr.visit(Application.make(V(translate(listOf(), tr)), args))
        }
        val lam = tr.e.scope.varRef(tr.e.lambda.new())
        val newvar = tr.e.newVar()
        val v = tr.e.lambda.eval(lam, newvar)
        val b = tr.e.scope.with(newvar, v.safety) {
            PropDeclare.translate(listOf(), tr)
        }
        tr.e.addMeaning(lam, newvar, b)
        return lam
    }
}

val TotalElementDeclare = ElementDeclare(false)
val PartialElementDeclare = ElementDeclare(true)
val TotalFuncDeclare = FuncDeclare(false)
val PartialFuncDeclare = FuncDeclare(true)

object LamFuncDeclare : Builtin {

    override fun translate(args: List<Exp<SafeVariable>>, tr: Translator): SafeVariable {
        if (args.size > 1) {
            return tr.visit(Application.make(V(translate(args.take(1), tr)), args.drop(1)))
        }
        assert(args.size == 1)

        val output = tr.e.scope.varRef(tr.e.lambda.new())

        val lf = tr.visit(args[0])
        val memberOfOut = tr.e.lambda.eval(output, tr.e.newVar())

        val outputSet = tr.e.applyLam(lf, memberOfOut)
        val memberOfOutputSet = tr.e.set.eval(outputSet, tr.e.newVar())

        val classLit = ClassLit(Sign.Positive, Environment.lamInter, listOf(output.term, memberOfOut.term, memberOfOutputSet.term))
        val choiceHead = ChoiceHead(listOf(ChoiceElement(classLit, (output.safety + outputSet.safety + memberOfOutputSet.safety).toList())), 1, 1)

        val rule = Rule(choiceHead, memberOfOut.safety.toList())
        tr.e.aspProg.add(rule)
        return output
    }

    override val type = asScheme("(a -> Set b) -> (a -> b)")
}