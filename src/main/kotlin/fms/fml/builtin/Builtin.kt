package fms.fml.builtin

import fms.asp.Abs
import fms.asp.AggBody
import fms.asp.AggElement
import fms.asp.Aggregate
import fms.asp.AggregateFunction
import fms.asp.AggregateLit
import fms.asp.Binop
import fms.asp.BuiltinNaf
import fms.asp.ClassNaf
import fms.asp.Herbrand
import fms.asp.LitBody
import fms.asp.Optimize
import fms.asp.OptimizeFunction
import fms.asp.RangeTerm
import fms.asp.Sign
import fms.asp.Term
import fms.asp.UnaryMinus
import fms.fml.expression.Application
import fms.fml.expression.Exp
import fms.fml.expression.Lambda
import fms.fml.expression.V
import fms.fml.type.Scheme
import fms.parsers.asExpr
import fms.parsers.asScheme
import fms.translation.SafeVariable
import fms.translation.Translator

interface Builtin {
    fun translate(args: List<Exp<SafeVariable>>, tr: Translator): SafeVariable
    val type: Scheme

    fun translateSet(args: List<Exp<SafeVariable>>, tr: Translator): SafeVariable {
        return tr.e.set.eval(translate(args, tr), tr.e.newVar())
    }

    fun translateBool(args: List<Exp<SafeVariable>>, tr: Translator): SafeVariable {
        return tr.e.bool.eval(translate(args, tr), Term.tuple())
    }
}

interface BoolBuiltin : Builtin {
    override fun translate(args: List<Exp<SafeVariable>>, tr: Translator): SafeVariable {
        return tr.e.asBool(translateBool(args, tr).safety)
    }

    override fun translateBool(args: List<Exp<SafeVariable>>, tr: Translator): SafeVariable
}

interface SetBuiltin : Builtin {
    override fun translate(args: List<Exp<SafeVariable>>, tr: Translator): SafeVariable {
        return tr.e.scope.varRef(tr.e.set.union(translateSet(args, tr)))
    }

    override fun translateSet(args: List<Exp<SafeVariable>>, tr: Translator): SafeVariable
}

// //////////

object TrueBuiltin : BoolBuiltin {
    override val type = asScheme("Bool")

    override fun translate(args: List<Exp<SafeVariable>>, tr: Translator): SafeVariable {
        assert(args.size == 0)
        return SafeVariable(Herbrand("true"))
    }

    override fun translateBool(args: List<Exp<SafeVariable>>, tr: Translator): SafeVariable {
        assert(args.size == 0)
        return SafeVariable(Term.tuple())
    }
}

object And : BoolBuiltin {
    override val type = asScheme("Bool -> Bool -> Bool")

    override fun translateBool(args: List<Exp<SafeVariable>>, tr: Translator): SafeVariable {
        assert(args.size == 2)
        val arg1 = tr.boolTrans().visit(args[0])
        val arg2 = tr.boolTrans().visit(args[1])
        return SafeVariable(Term.tuple(), arg1.safety + arg2.safety)
    }
}

object Or : Builtin {
    override val type = asScheme("Bool -> Bool -> Bool")

    override fun translate(args: List<Exp<SafeVariable>>, tr: Translator): SafeVariable {
        val arg1 = tr.boolTrans().visit(args[0])
        val arg2 = tr.boolTrans().visit(args[1])
        val or = tr.e.bool.union(arg1, arg2)
        return tr.e.scope.varRef(or)
    }
}

object Not : BoolBuiltin {
    override val type = asScheme("Bool -> Bool")

    override fun translateBool(args: List<Exp<SafeVariable>>, tr: Translator): SafeVariable {
        assert(args.size == 1)
        val arg = tr.visit(args[0])
        return tr.e.bool.notEval(arg, Term.tuple())
    }
}

object UnaryMinusB : Builtin {
    override val type = asScheme("Int -> Int")

    override fun translate(args: List<Exp<SafeVariable>>, tr: Translator): SafeVariable {
        assert(args.size == 1)
        val arg = tr.visit(args.first())
        return SafeVariable(UnaryMinus(arg.term), arg.safety)
    }
}

object Forall : Builtin {
    override val type = asScheme("Set a -> (a -> Bool) -> Bool")

    override fun translate(args: List<Exp<SafeVariable>>, tr: Translator): SafeVariable {
        // Define forall as the dual of the exists operator
        assert(args.size == 2)
        val all = tr.visit(Application.make(asExpr("\\s f -> ~(? s (\\x -> ~(f x)))").cast(), args))
        return all
    }
}

object Exists : BoolBuiltin {

    override val type = asScheme("Set a -> (a -> Bool) -> Bool")

    override fun translateBool(args: List<Exp<SafeVariable>>, tr: Translator): SafeVariable {
        val set = tr.setTrans().visit(args[0])
        val func = args[1]

        val v = set.term.freeVars()
        val outp = tr.e.scope.with(v.map { it to set.safety }) {
            if (func is Lambda) {
                tr.boolTrans().visit(func.f.instantiate { V(set) })
            } else {
                tr.boolTrans().visit(Application.make(func, V(set)))
            }
        }
        return outp
    }
}

object All : BoolBuiltin {
    override val type = asScheme("Set Bool -> Bool")

    override fun translateBool(args: List<Exp<SafeVariable>>, tr: Translator): SafeVariable {
        assert(args.size == 1)
        val arg = tr.setTrans().visit(args[0])
        val notAll = tr.e.bool.union(tr.e.bool.notEval(arg, Term.tuple()))
        return Not.translateBool(listOf(V(tr.e.scope.varRef(notAll))), tr)
    }
}

object Member : BoolBuiltin {

    override val type = asScheme("a -> Set a -> Bool")

    override fun translateBool(args: List<Exp<SafeVariable>>, tr: Translator): SafeVariable {
        assert(args.size == 2)
        val elTranslation = tr.visit(args[0])
        val setTranslation = tr.visit(args[1])
        val safety = tr.e.set.eval(setTranslation, elTranslation.term).safety + elTranslation.safety
        return SafeVariable(Term.tuple(), safety)
    }
}

object Range : SetBuiltin {

    override val type = asScheme("Int -> Int -> Set Int")

    override fun translateSet(args: List<Exp<SafeVariable>>, tr: Translator): SafeVariable {
        val out = tr.e.newVar()
        val fromT = tr.visit(args[0])
        val toT = tr.visit(args[1])
        return SafeVariable(out,
                fromT.safety + toT.safety +
                        setOf(LitBody(BuiltinNaf(out, Binop.EQ, RangeTerm(fromT.term, toT.term)))))
    }
}

object AbsoluteValue : Builtin {

    override val type = asScheme("Int -> Int")

    override fun translate(args: List<Exp<SafeVariable>>, tr: Translator): SafeVariable {
        assert(args.size == 1)
        val from = tr.visit(args[0])
        return SafeVariable(Abs(from.term), from.safety)
    }
}

data class TransAggregate(val aggF: AggregateFunction, override val type: Scheme) : Builtin {

    override fun translate(args: List<Exp<SafeVariable>>, tr: Translator): SafeVariable {
        assert(args.size == 1)
        val set = tr.visit(args[0])
        val memterm = tr.e.newVar()
        val mem = tr.e.set.classLit(listOf(set.term, memterm))
        val out = Aggregate(aggF, listOf(AggElement(listOf(memterm), listOf(LitBody(ClassNaf(Sign.Positive, mem))))))

        val varOut = tr.e.newVar()
        val eq = AggBody(false, AggregateLit(out, Binop.EQ, varOut))

        return tr.e.makeTseitin(SafeVariable(varOut, setOf(eq) + set.safety))
    }

    override fun toString(): String {
        return aggF.toString()
    }
}

val Count = TransAggregate(AggregateFunction.AggCount, asScheme("Set a -> Int"))
val MinBuiltin = TransAggregate(AggregateFunction.AggMin, asScheme("Set a -> a"))
val MaxBuiltin = TransAggregate(AggregateFunction.AggMax, asScheme("Set a -> a"))
val SumBuiltin = TransAggregate(AggregateFunction.AggSum, asScheme("Set Int -> Int"))

object SumBy : Builtin {
    override val type = asScheme("(a -> Int) -> Set a -> Int")

    override fun translate(args: List<Exp<SafeVariable>>, tr: Translator): SafeVariable {
        assert(args.size == 2)
        val set = tr.visit(args[1])
        val memterm = tr.e.newVar()
        val mem = tr.e.set.classLit(listOf(set.term, memterm))
        val memV = tr.e.set.eval(set, memterm)
        val byTerm = tr.e.scope.with(memterm, memV.safety) {
            tr.visit(Application(args[0], V(memV)))
        }
        val out = Aggregate(AggregateFunction.AggSum, listOf(AggElement(listOf(byTerm.term, memterm),
                listOf(LitBody(ClassNaf(Sign.Positive, mem))) + (byTerm.safety - memV.safety))))

        val varOut = tr.e.newVar()
        val eq = AggBody(false, AggregateLit(out, Binop.EQ, varOut))

        return tr.e.makeTseitin(SafeVariable(varOut, setOf(eq)))
    }
}

object Ite : Builtin {
    override val type = asScheme("Bool -> a -> a -> a")

    override fun translate(args: List<Exp<SafeVariable>>, tr: Translator): SafeVariable {
        assert(args.size == 3)
        val argtrans = tr.visit(args[0])
        val eif = tr.e.bool.eval(argtrans, Term.tuple())
        val eThen = tr.e.scope.withSafety(eif.safety) {
            val o = tr.visit(args[1])
            SafeVariable(o.term, o.safety + tr.e.scope.safety() + eif.safety)
        }

        val enif = tr.e.bool.notEval(argtrans, Term.tuple())
        val eElse = tr.e.scope.withSafety(enif.safety) {
            val o = tr.visit(args[2])
            SafeVariable(o.term, o.safety + tr.e.scope.safety() + enif.safety)
        }

        val herb = tr.e.tseitin.union(eThen, eElse)
        return tr.e.tseitin.eval(tr.e.scope.varRef(herb), tr.e.newVar())
    }
}

object Assert : Builtin {
    override val type = asScheme("Bool -> a -> a")

    override fun translate(args: List<Exp<SafeVariable>>, tr: Translator): SafeVariable {
        assert(args.size == 2)
        val eif = tr.boolTrans().visit(args[0])
        val eThen = tr.e.scope.withSafety(eif.safety) {
            val o = tr.visit(args[1])
            SafeVariable(o.term, o.safety + tr.e.scope.safety() + eif.safety)
        }
        return eThen
    }

    override fun translateSet(args: List<Exp<SafeVariable>>, tr: Translator): SafeVariable {
        assert(args.size == 2)
        val eif = tr.boolTrans().visit(args[0])
        val eThen = tr.e.scope.withSafety(eif.safety) {
            val o = tr.setTrans().visit(args[1])
            SafeVariable(o.term, o.safety + tr.e.scope.safety() + eif.safety)
        }
        return eThen
    }
}

object SetBind : SetBuiltin {
    override val type = asScheme("Set a -> (a -> Set b) -> Set b")

    override fun translateSet(args: List<Exp<SafeVariable>>, tr: Translator): SafeVariable {
        val setElem = tr.setTrans().visit(args[0])
        val sE = setElem.term.freeVars().map { it to setElem.safety }

        val out = args[1].let {
            if (it is Lambda) {
                tr.e.scope.with(sE) {
                    val out = tr.setTrans().visit(it.f.instantiate { V(setElem) })
                    return@with out
                }
            } else {
                val bindTrans = tr.visit(args[1])
                tr.e.scope.with(sE) {
                    val e = tr.e.set.eval(tr.e.applyLam(bindTrans, setElem), tr.e.newVar())
                    return@with e
                }
            }
        }
        return out
    }
}

data class OptimizeBuiltin(val direction: OptimizeFunction) : Builtin {
    // TODO: optimise should not occur in expressions
    override val type = asScheme("a -> Bool")

    override fun translate(args: List<Exp<SafeVariable>>, tr: Translator): SafeVariable {
        assert(args.size == 1)
        val set = tr.visit(args[0])
        val aggEl = AggElement(listOf(set.term), set.safety.toList())
        val stat = Optimize(direction, listOf(aggEl))
        tr.e.aspProg.add(stat)

        return SafeVariable(Herbrand("true"))
    }
}

val Minimize = OptimizeBuiltin(OptimizeFunction.Minimize)
val Maximize = OptimizeBuiltin(OptimizeFunction.Maximize)
