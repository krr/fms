package fms.fml.abstract

sealed class Var<B, A> {
    abstract fun <C> map(f: (A) -> C): Var<B, C>
}

data class BoundVar<B, A>(val b: B) : Var<B, A>() {
    override fun <C> map(f: (A) -> C): Var<B, C> = BoundVar(b)
}

data class FreeVar<B, A>(val a: A) : Var<B, A>() {
    override fun <C> map(f: (A) -> C): Var<B, C> = FreeVar(f(a))
}

class Named<N, B>(val name: N, val b: B) {
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as Named<*, *>

        if (b != other.b) return false

        return true
    }

    override fun hashCode(): Int {
        return b?.hashCode() ?: 0
    }

    override fun toString(): String {
        return "Named(name=$name, b=$b)"
    }

    fun copy(name: N?, b: B?): Named<N, B> {
        val nName = name ?: this.name
        val nB = b ?: this.b
        return Named(nName, nB)
    }
}
