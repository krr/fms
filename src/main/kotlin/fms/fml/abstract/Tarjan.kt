package fms.fml.abstract

import java.util.Stack

typealias Nodes<A> = List<Node<A>>

class Node<A>(val n: A) {
    var index = -1 // -1 signifies undefined
    var lowLink = -1
    var onStack = false

    override fun toString() = n.toString()
}

class DirectedGraph<A>(val vs: Nodes<A>, val es: Map<Node<A>, Nodes<A>>) {
    override fun toString(): String {
        return "$vs \n $es"
    }
}

fun <A> tarjan(g: DirectedGraph<A>): List<Nodes<A>> {
    val sccs = mutableListOf<Nodes<A>>()
    var index = 0
    val s = Stack<Node<A>>()

    fun strongConnect(v: Node<A>) {
        // Set the depth index for v to the smallest unused index
        v.index = index
        v.lowLink = index
        index++
        s.push(v)
        v.onStack = true

        // consider successors of v
        for (w in g.es[v]!!) {
            if (w.index < 0) {
                // Successor w has not yet been visited; recurse on it
                strongConnect(w)
                v.lowLink = minOf(v.lowLink, w.lowLink)
            } else if (w.onStack) {
                // Successor w is in stack s and hence in the current SCC
                v.lowLink = minOf(v.lowLink, w.index)
            }
        }

        // If v is a root node, pop the stack and generate an SCC
        if (v.lowLink == v.index) {
            val scc = mutableListOf<Node<A>>()
            do {
                val w = s.pop()
                w.onStack = false
                scc.add(w)
            } while (w != v)
            sccs.add(scc)
        }
    }

    for (v in g.vs) if (v.index < 0) strongConnect(v)
    return sccs
}