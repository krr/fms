package fms.fml.expression

class LocalNameProvider(val prefix: String) {
    private var fresh = 0
    fun name(): String {
        if (fresh < 26) {
            return prefix + ('a'.toInt() + (fresh++)).toChar().toString()
        }
        return prefix + "${fresh++}"
    }
}