package fms.fml.expression.visitor

import fms.fml.expression.Alt
import fms.fml.expression.Application
import fms.fml.expression.Case
import fms.fml.expression.HerbrandExp
import fms.fml.expression.Injection
import fms.fml.expression.Lambda
import fms.fml.expression.LangBuiltin
import fms.fml.expression.Let
import fms.fml.expression.OutputExp
import fms.fml.expression.SetExp
import fms.fml.expression.V
import fms.fml.expression.original

object RecoverOriginal : ExpVisitor<String, String> {

    override fun visitV(exp: V<String>): String {
        return exp.a
    }

    override fun visitApplication(exp: Application<String>): String {
        return "(" + visit(exp.f) + " " + visit(exp.x) + ")"
    }

    override fun visitLambda(exp: Lambda<String>): String {
        return "\\${exp.v} -> ${visit(exp.f.original)}"
    }

    override fun visitLet(exp: Let<String>): String {
        var out = "let "
        out += exp.binds.map { it.name + ":=" + it.exp.original }.toString()
        out += " in "
        out += exp.exp.original
        return out
    }

    override fun visitInjection(exp: Injection<String, Any>): String {
        return exp.value.toString()
    }

    override fun visitCase(exp: Case<String>): String {
        return "case ${visit(exp.exp)} of ${exp.alts.map { visitAlt(it) }}"
    }

    fun visitAlt(alt: Alt<String>): String {
        return "${alt.pattern} -> ${visit(alt.exp.original)}"
    }

    override fun visitLangBuiltin(exp: LangBuiltin<String>): String {
        return exp.builtin.javaClass.name
    }

    override fun visitSet(exp: SetExp<String>): String {
        return exp.content.map { visit(it) }.toString()
    }

    override fun visitHerbrandExp(exp: HerbrandExp<String>): String {
        return "${exp.name}${exp.args.map { visit(it) }}"
    }

    override fun visitOutputExp(exp: OutputExp<String>): String {
        return "[${exp.name}](${exp.actualExp})"
    }
}