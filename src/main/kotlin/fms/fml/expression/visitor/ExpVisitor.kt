package fms.fml.expression.visitor

import fms.fml.expression.Application
import fms.fml.expression.Case
import fms.fml.expression.Exp
import fms.fml.expression.HerbrandExp
import fms.fml.expression.Injection
import fms.fml.expression.Lambda
import fms.fml.expression.LangBuiltin
import fms.fml.expression.Let
import fms.fml.expression.OutputExp
import fms.fml.expression.SetExp
import fms.fml.expression.V

interface ExpVisitor<A, out T> {
    fun visitV(exp: V<A>): T = default(exp)
    fun visitApplication(exp: Application<A>): T = default(exp)
    fun visitLambda(exp: Lambda<A>): T = default(exp)
    fun visitLet(exp: Let<A>): T = default(exp)
    fun visitInjection(exp: Injection<A, Any>): T = default(exp)
    fun visitCase(exp: Case<A>): T = default(exp)
    fun visitLangBuiltin(exp: LangBuiltin<A>): T = default(exp)
    fun visitSet(exp: SetExp<A>): T = default(exp)
    fun visitHerbrandExp(exp: HerbrandExp<A>): T = default(exp)
    fun visitOutputExp(exp: OutputExp<A>): T = default(exp)

    @Suppress("UNCHECKED_CAST")
    fun visit(exp: Exp<A>): T = when (exp) {
        is V -> visitV(exp)
        is Application -> visitApplication(exp)
        is Lambda -> visitLambda(exp)
        is Let -> visitLet(exp)
        is Injection<*, *> -> visitInjection(exp as Injection<A, Any>)
        is Case -> visitCase(exp)
        is LangBuiltin -> visitLangBuiltin(exp)
        is SetExp -> visitSet(exp)
        is HerbrandExp -> visitHerbrandExp(exp)
        is OutputExp -> visitOutputExp(exp)
    }

    fun default(exp: Exp<A>): T {
        throw Error("No default visitor value specified")
    }
}

fun <A, T> ExpVisitor<A, T?>.visitJust(exp: Exp<A>): T {
    return this.visit(exp) ?: throw Error("Expected a result")
}