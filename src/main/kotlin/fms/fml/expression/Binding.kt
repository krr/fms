package fms.fml.expression

import fms.fml.type.Scheme

/**
 * This represents a binding of a patttern "pat" to an expression "exp"
 */
data class Binding(val pat: Pattern, val exp: Exp<String>, val type: Scheme?, val pragma: BindPragma) {

    // Base comes first!
    fun toUBind(fresh: LocalNameProvider): List<SBinding> {
        if (pat is VarPattern) {
            var oExp = exp
            if (pragma.print == true) {
                oExp = OutputExp(pat.name, exp)
                pragma.print = null
            }
            return listOf(SBinding(pat.name, oExp, type, pragma))
        }
        val local = fresh.name()
        val baseBind = SBinding(local, exp, type, pragma)
        val bVars = pat.varList().map {
            var exp: Expression = Case(V(local), listOf(Alt.make(pat.keepOnly(it), V(it))))
            if (pragma.print == true) {
                exp = OutputExp(it, exp)
            }
            SBinding(it, exp, type, pragma)
        }
        pragma.print = null
        return listOf(baseBind) + bVars
    }
}

/**
 * This represents a binding of a pattern "pat" to a scoped expression "exp"
 */
data class Alt<A>(val pattern: Pattern, val exp: Scoped<A>) {
    companion object {
        fun make(pattern: Pattern, exp: Exp<String>): Alt<String> =
                Alt(pattern, exp.abstractPattern(pattern))
    }

    fun <B> bindModule(f: (A) -> Exp<B>): Alt<B> =
            Alt(pattern, exp.bindModule(f))
}

/**
 * This represents a binding of a single variable "name" to an expression "exp"
 */
data class SBinding(val name: String, val exp: Exp<String>, val type: Scheme?, val pragma: BindPragma) {
    fun toBind(): Bind<String> {
        return Bind(name, exp.abstractOne(name), type, pragma)
    }

    fun toBind(allVars: List<String>): Bind<String> {
        return Bind(name, exp.abstractList(allVars), type, pragma)
    }
}

/**
 * This represents a binding of a single variable "name" to a scoped expression "exp
 */
data class Bind<A>(val name: String, val exp: Scoped<A>, val type: Scheme?, val pragma: BindPragma) {
    fun <B> bindModule(f: (A) -> Exp<B>): Bind<B> =
            Bind(name, exp.bindModule(f), type, pragma)

    override fun toString(): String {
        return "($name,$exp)"
    }

    fun <B> mapExp(f: (Scoped<A>) -> Scoped<B>): Bind<B> {
        return Bind(name, f(exp), type, pragma)
    }
}

data class BindPragma(
    var inline: Boolean? = null,
    var print: Boolean? = null,
    var strict: Boolean = false
)