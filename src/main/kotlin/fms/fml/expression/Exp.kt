package fms.fml.expression

import fms.fml.abstract.BoundVar
import fms.fml.abstract.FreeVar
import fms.fml.abstract.Named
import fms.fml.abstract.Var
import fms.fml.builtin.Builtin
import fms.fml.builtin.Not
import fms.fml.expression.visitor.RecoverOriginal
import fms.fml.type.TypeRef
import fms.translation.InvalidExpressionException
import org.antlr.v4.runtime.ParserRuleContext

/**
 * We represent Bound Variables with a combination of De Bruijne indices and their original naming
 */
typealias VarT = Named<String, Int>

/**
 * Traditional expressions have Strings as free variables
 */
typealias Expression = Exp<String>

/**
 * A normal scoped expression has variables represented by VarT
 */
typealias Scoped<A> = ScopedExp<VarT, A>

val Scoped<String>.original: Exp<String>
    get() = this.instantiate { V(it.name) }

fun <B, A> Exp<Var<B, Exp<A>>>.push(): Exp<Var<B, A>> = this.bind<Var<B, A>> {
    when (it) {
        is BoundVar -> V(BoundVar(it.b))
        is FreeVar -> it.a.liftM { FreeVar<B, A>(it) }
    }
}

/**
 * A ScopedExp has variables with Expression
 */
@Suppress("UNCHECKED_CAST")
data class ScopedExp<B, A> private constructor(val unscope: Exp<Var<B, A>>) {

    companion object {
        fun <B, A> make(exp: Exp<Var<B, A>>): ScopedExp<B, A> {
            return ScopedExp(exp)
        }
    }

    fun <C> bindModule(f: (A) -> Exp<C>): ScopedExp<B, C> = ScopedExp(unscope.bind<Var<B, C>> {
        when (it) {
            is BoundVar -> V(BoundVar<B, C>(it.b))
            is FreeVar -> f(it.a).emptyScoped()
        }
    })

    fun instantiate(f: (B) -> Exp<A>): Exp<A> = unscope.bind {
        when (it) {
            is BoundVar -> f(it.b)
            is FreeVar -> V(it.a)
        }
    }

    fun boundVars(): List<B> {
        val bounds = mutableSetOf<B>()
        instantiate {
            bounds.add(it)
            // This doesn't matter as long as its an Exp<A>
            LangBuiltin(Not)
        }
        return bounds.toList()
    }
}

fun <B, A> ScopedExp<B, A>.emptyInstantiate(): Exp<A> {
    return this.instantiate { throw Error("Empty Instantiation with Free Variables") }
}

/**
 * This class represents expressions of which the free variables are represented by an object of type A
 */
sealed class Exp<A> : Located {
    /**
     * Replace the free variables of type "A" with a full expression "Exp<B>"
     * This mapping is provided by the function "f"
     */
    abstract fun <B> bindModuleA(f: (A) -> Exp<B>): Exp<B>

    fun <B> bind(f: (A) -> Exp<B>): Exp<B> {
        return bindModuleA(f).also {
            it.location = this.location
            it.typeRef = this.typeRef
        }
    }

    /**
     * Casts this expression to another type. This can only be done if there are no free variables
     * (If there are free variables, these can't be automatically converted to a new type)
     * @pre There are no free variables
     */
    fun <B> cast(): Exp<B> = this.liftM { throw Exception("Couldn't cast because of free variable $it") }

    fun <B> liftM(f: (A) -> B): Exp<B> = bind { V(f(it)) }

    /**
     * Abstract some of the variables "A", identified by a "B" through a partial function "f"
     */

    fun <B> abstract(f: (A) -> B?): ScopedExp<Named<A, B>, A> {
        val k = fun(a: A): Var<Named<A, B>, A> {
            val result = f(a)
            return when (result) {
                null -> FreeVar(a)
                else -> BoundVar(Named(a, result))
            }
        }
        return ScopedExp.make(this.liftM(k))
    }

    fun abstractList(vars: List<A>): ScopedExp<Named<A, Int>, A> {
        val vr = vars.sortedBy { it?.hashCode() }
        val duplicates = vr.filterIndexed { i, e -> i != vr.lastIndex && e == vr[i + 1] }
        if (duplicates.isNotEmpty()) {
            throw InvalidExpressionException("Double binding of variable(s) $duplicates")
        }
        return this.abstract { v ->
            vars.indexOf(v).let {
                when (it) {
                    -1 -> null
                    else -> it
                }
            }
        }
    }

    fun abstractPattern(pat: APattern<A>) =
            abstractList(pat.varList())

    /**
     * Abstract a single variable "s", leave the other variables free
     */
    fun abstractOne(s: A): ScopedExp<Named<A, Int>, A> =
            abstractList(listOf(s))

    override var location: ParserRuleContext? = null
    override var typeRef: TypeRef = TypeRef()

    fun <B> emptyScope(): ScopedExp<B, A> {
        return ScopedExp.make(emptyScoped())
    }

    fun <B> emptyScoped(): Exp<Var<B, A>> =
            this.liftM { FreeVar<B, A>(it) as Var<B, A> }

    fun freeVars(): List<A> {
        val out = mutableListOf<A>()
        this.liftM { out.add(it) }
        return out
    }

    override fun toString(): String {
        val s: Expression = bind { V(it.toString()) }
        return RecoverOriginal.visit(s)
    }
}

/**
 * This class is a variable (of type A)
 */
data class V<A>(val a: A) : Exp<A>() {
    override fun <B> bindModuleA(f: (A) -> Exp<B>): Exp<B> = f(a)
    override fun toString(): String {
        return super.toString()
    }
}

/**
 * This class represents the application of a function "f" to an argument "x"
 */
data class Application<A>(val f: Exp<A>, val x: Exp<A>) : Exp<A>() {
    override fun <B> bindModuleA(f: (A) -> Exp<B>): Exp<B> = Application(this.f.bind(f), x.bind(f))

    fun collectArgs(): Pair<Exp<A>, List<Exp<A>>> {
        var f = this.f
        var xs = mutableListOf<Exp<A>>()
        xs.add(x)
        while (f is Application) {
            xs.add(f.x)
            f = f.f
        }
        return Pair(f, xs.reversed())
    }

    override fun toString(): String {
        return super.toString()
    }

    companion object {
        fun <A> make(f: Exp<A>, xs: Iterable<Exp<A>>): Exp<A> =
                xs.fold(f, ::Application)

        fun <A> make(f: Exp<A>, vararg xs: Exp<A>): Exp<A> =
                xs.fold(f, ::Application)
    }
}

/**
 * This is a unary lambda function wich abstracts the variable "v" in the expression "f"
 */
data class Lambda<A>(val v: String, val f: Scoped<A>) : Exp<A>() {
    override fun <B> bindModuleA(f: (A) -> Exp<B>): Exp<B> = Lambda(v, this.f.bindModule(f))

    override fun toString() = super.toString()

    companion object {
        fun make(p: Pattern, f: Expression): Lambda<String> {
            if (p is VarPattern) {
                return make(p.name, f)
            }
            if (p is AnonymVar) {
                return make(LocalNameProvider("_").name(), f)
            }
            val name = LocalNameProvider("_").name()
            return Lambda.make(name, Case(V(name), listOf(Alt.make(p, f))))
        }

        fun make(s: String, f: Expression): Lambda<String> =
                Lambda(s, f.abstractOne(s))
    }
}

/**
 * This binds a sets of bindings "Binds" to a scoped expression "exp"
 */
data class Let<A>(val binds: List<Bind<A>>, val exp: Scoped<A>) : Exp<A>() {
    override fun <B> bindModuleA(f: (A) -> Exp<B>): Exp<B> =
            Let(binds.map { it.bindModule(f) }, exp.bindModule(f))

    override fun toString() = super.toString()

    companion object {
        fun make(bindings: List<Binding>, exp: Expression): Exp<String> {
            val fresh = LocalNameProvider("_")
            val newBinds = bindings.flatMap { it.toUBind(fresh) }
            val allVars = newBinds.map { it.name }
            val binds = newBinds.map { it.toBind(allVars) }
            return Let(binds, exp.abstractList(allVars))
        }
    }
}

/**
 * An injection of a Kotlin object into the Expression tree
 * In general this is only used for Strings, Integers and Bools
 */
data class Injection<A, out B>(val value: B) : Exp<A>() {
    override fun toString() = super.toString()

    override fun <B> bindModuleA(f: (A) -> Exp<B>): Exp<B> =
            Injection(value)
}

/**
 * Deconstruct an expression "exp" into alternatives "alts"
 */
data class Case<A>(val exp: Exp<A>, val alts: List<Alt<A>>) : Exp<A>() {
    override fun toString() = super.toString()

    override fun <B> bindModuleA(f: (A) -> Exp<B>): Exp<B> =
            Case(exp.bind(f), alts.map { it.bindModule(f) })
}

/**
 * A language bulitin which is injected into the language
 */
data class LangBuiltin<A>(val builtin: Builtin) : Exp<A>() {
    override fun toString(): String {
        return super.toString()
    }

    override fun <B> bindModuleA(f: (A) -> Exp<B>): Exp<B> =
            LangBuiltin(builtin)
}

/**
 * A set defined through a set command
 */
data class SetExp<A>(val content: List<Exp<A>>) : Exp<A>() {
    override fun toString() = super.toString()

    companion object {
        fun make(content: List<Expression>): Exp<String> {
            return SetExp(content)
        }

        fun make(vararg content: Expression): Exp<String> {
            return make(content.toList())
        }
    }

    override fun <B> bindModuleA(f: (A) -> Exp<B>): Exp<B> =
            SetExp(content.map { it.bind(f) })
}

/**
 * An applied constructor with the name "name" applied to a list of expressions "args"
 */
data class HerbrandExp<A>(val name: String, val args: List<Exp<A>> = listOf()) : Exp<A>() {
    override fun toString() = super.toString()

    override fun <B> bindModuleA(f: (A) -> Exp<B>): Exp<B> =
            HerbrandExp(name, args.map { it.bind(f) })

    companion object {
        fun <A> tuple(tup: List<Exp<A>>): Exp<A> {
            return if (tup.size == 1) {
                tup[0]
            } else {
                HerbrandExp("", tup)
            }
        }
    }
}

/**
 * A expression "actualExp" which needs to be outputted under a particular name "name"
 */
data class OutputExp<A>(val name: String, val actualExp: Exp<A>) : Exp<A>() {
    override fun toString() = super.toString()

    override fun <B> bindModuleA(f: (A) -> Exp<B>): Exp<B> =
            OutputExp(name, actualExp.bind(f))
}
