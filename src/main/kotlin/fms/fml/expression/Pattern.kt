package fms.fml.expression

import fms.asp.NafLit
import fms.asp.Term
import fms.fml.type.Apply
import fms.fml.type.NewTypeProvider
import fms.fml.type.Scheme
import fms.fml.type.Type
import fms.unions

typealias Pattern = APattern<String>

sealed class APattern<A> {
    abstract fun varList(vars: MutableList<A> = mutableListOf()): List<A>
    abstract fun <B> map(f: (A) -> B): APattern<B>
    abstract fun isTrivial(): Boolean
    abstract fun isGround(): Boolean
    abstract fun readable(): String
    abstract fun type(fresh: NewTypeProvider): Pair<Map<A, Scheme>, Type>
    abstract fun keepOnly(varToKeep: A): APattern<A>
    abstract fun <B> match(vexp: Exp<B>): Map<A, Exp<B>>?
    abstract fun <B> canMatch(vexp: Exp<B>): Boolean
}

data class VarPattern<A>(val name: A) : APattern<A>() {
    override fun <B> canMatch(vexp: Exp<B>): Boolean {
        return true
    }

    override fun <B> match(vexp: Exp<B>): Map<A, Exp<B>>? {
        return mapOf(name to vexp)
    }

    override fun keepOnly(varToKeep: A): APattern<A> {
        return if (varToKeep == name) {
            this
        } else {
            AnonymVar()
        }
    }

    override fun isGround(): Boolean = false
    override fun readable() = "$name"
    override fun isTrivial() = true
    override fun <B> map(f: (A) -> B): APattern<B> = VarPattern(f(name))
    override fun varList(vars: MutableList<A>): List<A> {
        vars += name
        return vars
    }

    override fun type(fresh: NewTypeProvider): Pair<Map<A, Scheme>, Type> {
        val t = fresh.type()
        return mapOf(name to t.scheme) to t
    }
}

data class Constructor<A>(val name: String, val args: List<APattern<A>>) : APattern<A>() {
    override fun <B> canMatch(vexp: Exp<B>): Boolean {
        if (vexp is HerbrandExp) {
            if (vexp.name == name && vexp.args.size == args.size) {
                return (args.zip(vexp.args)).all { (a, b) -> a.canMatch(b) }
            } else {
                return false
            }
        }
        return true
    }

    override fun <B> match(vexp: Exp<B>): Map<A, Exp<B>>? {
        if (vexp is HerbrandExp && vexp.name == name && vexp.args.size == args.size) {
            val matches = args.zip(vexp.args) { a, b -> a.match(b) ?: return null }
            return matches.unions()
        }
        return null
    }

    override fun keepOnly(varToKeep: A): APattern<A> {
        return Constructor(name, args.map { it.keepOnly(varToKeep) })
    }

    override fun isGround(): Boolean = args.all { it.isGround() }

    companion object {
        fun <A> tuple(tup: List<APattern<A>>): APattern<A> {
            return if (tup.size == 1) {
                tup[0]
            } else {
                Constructor("", tup)
            }
        }
    }

    override fun readable() = "$name + ${args.map { it.readable() }}"
    override fun isTrivial() = false
    override fun <B> map(f: (A) -> B): APattern<B> = Constructor(name, args.map { it.map(f) })
    override fun varList(vars: MutableList<A>): List<A> {
        args.forEach { it.varList(vars) }
        return vars
    }

    override fun type(fresh: NewTypeProvider): Pair<Map<A, Scheme>, Type> {
        val (env, ts) = args.map { it.type(fresh) }.unzip()
        val totEnv = env.unions()

        val t = if (name == "") {
            if (ts.isEmpty()) {
                Type.Unit
            } else {
                Apply.make(Type.Tuple, ts)
            }
        } else {
            fresh.type()
        }
        return totEnv to t
    }
}

data class Injected<A, out B>(val obj: B) : APattern<A>() {
    override fun <B> canMatch(vexp: Exp<B>): Boolean {
        if (vexp is Injection<*, *>) {
            return vexp.value == obj
        }
        return true
    }

    override fun <B> match(vexp: Exp<B>): Map<A, Exp<B>>? {
        if (vexp is Injection<*, *> && vexp.value == obj) {
            return mapOf()
        }
        return null
    }

    override fun keepOnly(varToKeep: A): APattern<A> {
        return this
    }

    override fun isGround(): Boolean = true
    override fun readable() = "$obj"
    override fun isTrivial() = false
    override fun <B> map(f: (A) -> B): APattern<B> = Injected(obj)
    override fun varList(vars: MutableList<A>) = vars

    override fun type(fresh: NewTypeProvider): Pair<Map<A, Scheme>, Type> {
        val t = when (obj) {
            is String -> Type.String
            is Int -> Type.Int
            is Boolean -> Type.Bool
            else -> throw Error("Unknown Injection")
        }
        return emptyMap<A, Scheme>() to t
    }
}

data class Comparison<A>(val pred: (Term) -> NafLit) : APattern<A>() {
    override fun <B> canMatch(vexp: Exp<B>): Boolean {
        return true
    }

    override fun <B> match(vexp: Exp<B>): Map<A, Exp<B>>? {
        return null
    }

    override fun keepOnly(varToKeep: A): APattern<A> {
        return this
    }

    override fun isGround(): Boolean = false
    override fun readable() = "comp"
    override fun varList(vars: MutableList<A>): List<A> =
            listOf()

    override fun <B> map(f: (A) -> B): APattern<B> =
            Comparison(pred)

    override fun isTrivial(): Boolean = false

    override fun type(fresh: NewTypeProvider): Pair<Map<A, Scheme>, Type> {
        val t = fresh.type()
        return emptyMap<A, Scheme>() to t
    }
}

class AnonymVar<A> : APattern<A>() {

    override fun <B> canMatch(vexp: Exp<B>): Boolean {
        return true
    }

    override fun <B> match(vexp: Exp<B>): Map<A, Exp<B>>? {
        return mapOf()
    }

    override fun keepOnly(varToKeep: A): APattern<A> {
        return this
    }

    override fun isGround(): Boolean = false
    override fun readable() = "_"
    override fun isTrivial() = true
    override fun <B> map(f: (A) -> B): APattern<B> = AnonymVar()
    override fun varList(vars: MutableList<A>) = vars

    override fun equals(other: Any?): Boolean {
        return other is AnonymVar<*>
    }

    override fun type(fresh: NewTypeProvider): Pair<Map<A, Scheme>, Type> {
        val t = fresh.type()
        return emptyMap<A, Scheme>() to t
    }
}
