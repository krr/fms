package fms.fml.expression

import fms.fml.type.TypeRef
import org.antlr.v4.runtime.ParserRuleContext

interface Located {
    var location: ParserRuleContext?
    var typeRef: TypeRef

    fun locate(x: ParserRuleContext?) {
        if (location == null) {
            location = x
        }
    }
}
