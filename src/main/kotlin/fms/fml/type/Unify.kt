package fms.fml.type

import fms.globalLog
import org.antlr.v4.runtime.ParserRuleContext

sealed class TypeException(val msg: String, var location: ParserRuleContext? = null) : Exception() {
    override val message: String?
        get() = "$locMsg$msg"

    val locMsg: String
        get() = location?.let { "At ${it.start.text} (${it.start.line}:${it.start.startIndex}): " } ?: ""
}

class UnableToUnify(t1: Type, t2: Type) : TypeException("Unable to unify ${t1 to t2}")
class InfiniteBind(tVar: Var, type: Type) : TypeException("Infinite bind ${tVar to type}")

fun unify(t1: Type, t2: Type): Subst = when {
    t1 is Apply && t2 is Apply -> {
        val s1 = unify(t1.t1, t2.t1)
        val s2 = unify(apply(s1, t1.t2), apply(s1, t2.t2))
        s2 compose s1
    }
    t1 is Var -> bind(t1, t2)
    t2 is Var -> bind(t2, t1)
    t1 is Const && t2 is Const && t1 == t2 -> emptySubst
    else -> {
        globalLog.trace { "Unable to unify $t1 $t2" }
        throw UnableToUnify(t1, t2)
    }
}

fun bind(tVar: Var, type: Type): Subst = when {
    tVar == type -> emptySubst
    occursIn(tVar, type) -> {
        globalLog.trace { "InfiniteBind $tVar $type" }
        throw InfiniteBind(tVar, type)
    }
    else -> mapOf(tVar.name to type)
}

fun occursIn(tVar: Var, type: Type): Boolean = when (type) {
    is Apply -> occursIn(tVar, type.t1) || occursIn(tVar, type.t2)
    is Var -> type == tVar
    is Const -> false
}
