package fms.fml.type

sealed class Type {

    companion object {
        fun Arrow(vararg t1: Type): Type {
            return t1.dropLast(1).foldRight(t1.last()) { a, b -> Apply(Apply(Const(BaseType.Arrow), a), b) }
        }

        val Bool = Const(BaseType.Bool)
        val Int = Const(BaseType.Int)
        val String = Const(BaseType.String)
        val Unit = Const(BaseType.Unit)
        val Tuple = Const(BaseType.Tuple)

        fun Set(a: Type) =
                Apply(Const(BaseType.Set), a)
    }

    val scheme get() = Scheme(emptyList(), this)

    fun generalize(env: TypeEnvironment) = Scheme(
            freeTypeVariables.filterNot(env::isBound),
            this
    )

    fun canonical(env: TypeEnvironment = TypeEnvironment.empty): Type {
        return generalize(env).instantiate(NewTypeProvider())
    }

    fun unifiesWith(t: Type): Boolean {
        try {
            unify(this, t)
            return true
        } catch (e: Exception) {
            return false
        }
    }

    fun canonicEqual(t: Type): Boolean {
        return this.canonical() == t.canonical()
    }

    fun returnsSet(): Boolean {
        return if (this is Apply && this.t1 is Apply && this.t1.t1 == Const(BaseType.Arrow)) {
            this.t2.returnsSet()
        } else {
            this is Apply && this.t1 == Const(BaseType.Set)
        }
    }

    val leftMost: Type
        get() = run {
            if (this is Apply) {
                return@run t1.leftMost
            } else {
                return@run this
            }
        }

    val arity: Int
        get() = run {
            if (this is Apply && this.t1 is Apply && this.t1.t1 == Const(BaseType.Arrow)) {
                1 + this.t2.arity
            } else {
                0
            }
        }
}

data class Var(val name: String) : Type() {
    override fun toString() = name
}

data class Const(val t1: BaseType) : Type() {
    override fun toString() = t1.toString()
}

data class Apply(val t1: Type, val t2: Type) : Type() {

    companion object {
        fun make(f: Type, xs: Iterable<Type>): Type =
                xs.fold(f, ::Apply)

        fun make(f: Type, vararg xs: Type): Type =
                xs.fold(f, ::Apply)
    }

    override fun toString(): String {
        if (t1 is Apply && t1.t1 == Const(BaseType.Arrow)) {
            return "(${t1.t2} -> $t2)"
        }
        if (t1 is Const) {
            return "$t1 ($t2)"
        }
        return "($t1) $t2"
    }
}

enum class BaseType {
    Bool,
    Int,
    String,
    Arrow {
        override fun toString() = "->"
    },
    Set,
    Unit {
        override fun toString() = "()"
    },
    Tuple
}

data class Scheme(val names: List<String>, val type: Type) {
    override fun toString() = "$names => $type"

    fun instantiate(fresh: NewTypeProvider) = run {
        val namesP = names.map { fresh.type() }
        val namesZ = (names zip namesP).toMap()
        apply(namesZ, type)
    }
}
