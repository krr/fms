package fms.fml.type

class NewTypeProvider {
    private var fresh = 0
    fun type(): Type {
        if (fresh < 26) {
            return Var(('a'.toInt() + (fresh++)).toChar().toString())
        }
        return Var("t${fresh++}")
    }

    private var freshVar = 0
    fun variable(): String {
        return "v${fresh++}"
    }
}

class TypeEnvironment(private val mapping: Map<String, Scheme> = mapOf()) {

    companion object {
        val empty = TypeEnvironment()
    }

    fun get(name: String): Scheme? {
        return mapping[name]
    }

    fun set(name: String, scheme: Scheme): TypeEnvironment {
        return TypeEnvironment(mapping + (name to scheme))
    }

    fun map(fn: (Scheme) -> Scheme): TypeEnvironment {
        return TypeEnvironment(mapping.mapValues { it -> fn(it.value) })
    }

    override fun toString(): String {
        return "$mapping"
    }

    fun isBound(name: String): Boolean {
        return mapping.values.stream().anyMatch { it.freeTypeVariables.contains(name) }
    }

    fun remove(name: String): TypeEnvironment {
        return TypeEnvironment(mapping.minus(name))
    }
}
