package fms.fml.type

class TypeRef {
    var type: Scheme? = null

    fun type(fresh: NewTypeProvider): Type {
        return scheme(fresh).instantiate(fresh)
    }

    fun scheme(fresh: NewTypeProvider): Scheme {
        return type ?: fresh.type().scheme.also { type = it }
    }

    fun applySub(sub: Subst) {
        val t = type
        if (t != null) {
            type = apply(sub, t)
        }
    }

    override fun toString() = "$type"
}
