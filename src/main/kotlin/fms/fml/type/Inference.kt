package fms.fml.type

import fms.globalLog
import fms.fml.expression.Alt
import fms.fml.expression.Application
import fms.fml.expression.Case
import fms.fml.expression.Exp
import fms.fml.expression.HerbrandExp
import fms.fml.expression.Injection
import fms.fml.expression.Lambda
import fms.fml.expression.LangBuiltin
import fms.fml.expression.Let
import fms.fml.expression.OutputExp
import fms.fml.expression.SetExp
import fms.fml.expression.V
import fms.fml.expression.visitor.ExpVisitor

typealias TypingResult = Pair<Subst, Type>

class InferType(
    val fresh: NewTypeProvider = NewTypeProvider(),
    var env: TypeEnvironment = TypeEnvironment()
) : ExpVisitor<String, TypingResult> {

    companion object {
        fun boolCheck(exp: Exp<String>) {
            val (_, b) = InferType().visit(exp)
            unify(b, Type.Bool)
        }

        fun typeCheck(exp: Exp<String>): Type {
            return InferType().visit(exp).second
        }

        fun unifyAll(list: List<TypingResult>, environment: TypeEnvironment, fresh: NewTypeProvider): TypingResult {
            if (list.isEmpty()) {
                return emptySubst to fresh.type()
            }
            var env = environment
            val (subs, types) = list.unzip()
            var megaSub = subs.fold(emptySubst) { a, b -> a compose b }
            env = apply(megaSub, env)
            var type = types[0]
            types.drop(1).forEach { t ->
                val s = unify(t, type)
                megaSub = megaSub compose s
                type = apply(s, type)
                env = apply(s, env)
            }
            return megaSub to type
        }
    }

    fun <A> localEnv(f: InferType.() -> A): A {
        val bup = env
        val out: A = this.f()
        env = bup
        return@localEnv out
    }

    override fun visitV(exp: V<String>): TypingResult = localEnv {
        val name = exp.a
        val out = env.get(name)?.let { emptySubst to it.instantiate(fresh) }
                ?: throw NoSuchElementException(name)
        return@localEnv out
    }

    override fun visitApplication(exp: Application<String>): TypingResult = localEnv {
        val typeVariable = fresh.type()
        val (s1, expr1Type) = visit(exp.f)
        env = apply(s1, env)
        val (s2, expr2Type) = visit(exp.x)
        val s3 = unify(apply(s2, expr1Type), Type.Arrow(expr2Type, typeVariable))
        val fS = (s3 compose s2 compose s1)
        val out = fS to apply(fS, typeVariable)
        return@localEnv out
    }

    override fun visit(exp: Exp<String>): TypingResult {
        try {
            val (s, t) = super.visit(exp)
            val s2 = unify(exp.typeRef.type(fresh), t)
            val bigSub = s compose s2
            exp.typeRef.applySub(bigSub)
            val realType = apply(bigSub, t)
            globalLog.trace { "$exp typed as $realType under $bigSub" }
            if (realType.canonical(env) != exp.typeRef.type(fresh).canonical(env)) {
                throw UnableToUnify(realType, exp.typeRef.type(fresh))
            }
            return bigSub to realType
        } catch (te: TypeException) {
            if (exp.location != null && te.location == null) {
                te.location = exp.location
            }
            throw te
        }
    }

    override fun visitLambda(exp: Lambda<String>): TypingResult = localEnv {
        val argType = fresh.type()
        val newVar = fresh.variable()
        env = env.set(newVar, argType.scheme)
        val (s1, exprType) = visit(exp.f.instantiate { V(newVar) })
        val out = s1 to Type.Arrow(apply(s1, argType), exprType)
        return@localEnv out
    }

    override fun visitLet(exp: Let<String>): TypingResult = localEnv {
        globalLog.trace { "Environment Start: $env" }
        // Invent All Bindings
        val (names, bTypes) = exp.binds.map { b ->
            val preKnown = b.type
            val newName = fresh.variable()
            if (preKnown != null) {
                env = env.set(newName, preKnown)
                return@map newName to preKnown.type
            } else {
                val t = fresh.type()
                env = env.set(newName, t.scheme)
                return@map newName to t
            }
        }.unzip()

        var types = bTypes
        // Unify Bindings withSafety actual expression
        exp.binds.forEachIndexed { i, b ->
            val (sub, t2) = visit(b.exp.instantiate { V(names[it.b]) })
            val a = unify(types[i], t2)
            val fullSub = a compose sub

            env = apply(fullSub, env)
            types = types.map { apply(fullSub, it) }

            globalLog.trace { "Env: $env" }
            // Generalize bindings
            val newScheme = (env.get(names[i])
                    ?: throw Error("Couldn't find ${names[i]}")).instantiate(fresh).generalize(env.remove(names[i])).also {
                globalLog.trace { "Generalize ${b.name} as $it" }
            }
            env = env.set(names[i], newScheme)
        }

        // Type inner expression
        val (s, t) = visit(exp.exp.instantiate { V(names[it.b]) })

        return@localEnv s to t
    }

    override fun visitInjection(exp: Injection<String, Any>): TypingResult = localEnv {
        val out = when (exp.value) {
            is Int -> emptySubst to Const(BaseType.Int)
            is String -> emptySubst to Const(BaseType.String)
            is Boolean -> emptySubst to Const(BaseType.Bool)
            else -> {
                throw Error("Can't type this injection: ${exp.value}")
            }
        }

        return@localEnv out
    }

    override fun visitCase(exp: Case<String>): TypingResult = localEnv {
        val (ins, out) = exp.alts.map { visitAlt(it) }.unzip()
        val inI = visit(exp.exp)
        val (sIn, _) = unifyAll(ins + inI, env, fresh)
        val (sOut, tOut) = unifyAll(out, env, fresh)
        return@localEnv sIn compose sOut to tOut
    }

    override fun visitLangBuiltin(exp: LangBuiltin<String>): TypingResult = localEnv {
        val out = exp.builtin.type.instantiate(fresh)
        return@localEnv emptySubst to out
    }

    override fun visitSet(exp: SetExp<String>): TypingResult = localEnv {
        val exps = exp.content
        val (sub, type) = InferType.unifyAll(exps.map { InferType(fresh, env).visit(it) }, env, fresh)
        return@localEnv sub to Type.Set(type)
    }

    override fun visitHerbrandExp(exp: HerbrandExp<String>): TypingResult = localEnv {
        if (exp.name == "" && exp.args.isEmpty()) {
            return@localEnv emptySubst to Type.Unit
        }
        if (exp.name == "") {
            val (substs, types) = exp.args.map { visit(it) }.unzip()
            val sub = substs.fold(emptySubst, { a, b -> a compose b })
            val typ = Apply.make(Type.Tuple, types)
            return@localEnv sub to typ
        }
        // TODO: Constructed Type signatures
        val out = emptySubst to fresh.type()
        return@localEnv out
    }

    override fun visitOutputExp(exp: OutputExp<String>): TypingResult = localEnv {
        return@localEnv visit(exp.actualExp)
    }

    fun visitAlt(alt: Alt<String>): Pair<TypingResult, TypingResult> = localEnv {
        val (extraEnv, patType) = alt.pattern.type(fresh)

        val insts = alt.pattern.varList().map { p ->
            val newName = fresh.variable()
            env = env.set(newName, extraEnv[p]!!)
            V(newName)
        }
        val inst = alt.exp.instantiate { insts[it.b] }
        val out = visit(inst)
        return@localEnv (emptySubst to patType) to out
    }
}
