package fms.fml.type

typealias Subst = Map<String, Type>

val emptySubst: Subst = emptyMap()

infix fun <A, B> Map<A, B>.union(map: Map<A, B>) = map + this

infix fun Subst.compose(subst: Subst): Subst {
    val mut = this.toMutableMap()
    subst.forEach { (s, t) ->
        mut.merge(s, t, { t1, t2 -> apply(unify(t1, t2), t1) })
    }
    return mut.mapValues { apply(mut, it.value) }
}

fun apply(subst: Subst, scheme: Scheme) = run {
    val substP = scheme.names.foldRight(subst) { a, b -> b - a }
    Scheme(scheme.names, apply(substP, scheme.type))
}

fun apply(subst: Subst, type: Type): Type = when (type) {
    is Const -> type
    is Var -> subst.getOrDefault(type.name, type)
    is Apply -> Apply(apply(subst, type.t1), apply(subst, type.t2))
}

fun apply(subst: Subst, env: TypeEnvironment) = env.map { apply(subst, it) }

val Scheme.freeTypeVariables get() = type.freeTypeVariables - names.toSet()

val Type.freeTypeVariables: Set<String>
    get() = when (this) {
        is Const -> emptySet()
        is Var -> setOf(name)
        is Apply -> t1.freeTypeVariables union t2.freeTypeVariables
    }
