package fms.translation

import fms.asp.Body
import fms.asp.Herbrand
import fms.asp.LitBody
import fms.globalLog
import fms.fml.abstract.FreeVar
import fms.fml.expression.Alt
import fms.fml.expression.Application
import fms.fml.expression.Bind
import fms.fml.expression.Case
import fms.fml.expression.Exp
import fms.fml.expression.visitor.ExpVisitor
import fms.fml.expression.HerbrandExp
import fms.fml.expression.Injection
import fms.fml.expression.Lambda
import fms.fml.expression.LangBuiltin
import fms.fml.expression.Let
import fms.fml.expression.OutputExp
import fms.fml.expression.SetExp
import fms.fml.expression.V
import fms.fml.expression.emptyInstantiate

data class InvalidExpressionException(val explanation: String) : Exception(explanation)

class Translator(val c: IDPCommand, val e: Environment = Environment(c)) : ExpVisitor<SafeVariable, SafeVariable> {

    override fun visit(exp: Exp<SafeVariable>): SafeVariable {
        globalLog.trace { "Handling $exp" }
        return e.scope.filter(exp.freeVars()) {
            super.visit(exp)
        }
    }

    override fun visitOutputExp(exp: OutputExp<SafeVariable>): SafeVariable {
        val trans = visit(exp.actualExp)
        e.output(exp.name, trans, exp.typeRef)
        return trans
    }

    override fun visitHerbrandExp(exp: HerbrandExp<SafeVariable>): SafeVariable {
        val translations = exp.args.map { visit(it) }
        return SafeVariable(Herbrand(exp.name, translations.map { it.term }), translations.flatMap { it.safety }.toSet())
    }

    override fun visitLangBuiltin(exp: LangBuiltin<SafeVariable>): SafeVariable {
        return exp.builtin.translate(listOf(), this)
    }

    override fun visitV(exp: V<SafeVariable>): SafeVariable = exp.a

    override fun visitLambda(exp: Lambda<SafeVariable>): SafeVariable {
        val lam = e.scope.varRef(e.lambda.new())
        val argument = e.newVar()
        val safevar = SafeVariable(argument, e.lambda.eval(lam, argument).safety + lam.safety)
        e.scope.with(argument, safevar.safety) {
            e.addMeaning(lam, safevar.term, visit(exp.f.instantiate { V(safevar) }))
        }
        return lam
    }

    override fun visitApplication(exp: Application<SafeVariable>): SafeVariable {
        val (f, xs) = exp.collectArgs()
        return visitApplication(f, xs)
    }

    private fun visitApplication(f: Exp<SafeVariable>, xs: List<Exp<SafeVariable>>): SafeVariable {
        return when (f) {
            is V -> xs.fold(f.a) { func, arg -> e.applyLam(func, visit(arg)) }
            is LangBuiltin -> f.builtin.translate(xs, this)
            else -> {
                return visitApplication(V(visit(f)), xs)
            }
        }
    }

    override fun visitLet(exp: Let<SafeVariable>): SafeVariable {
        val newBinds = visitBinds(exp.binds)
        // Instantiate expression withSafety translations
        val newExp = exp.exp.instantiate { V(newBinds[it.b]) }
        return visit(newExp)
    }

    override fun visitInjection(exp: Injection<SafeVariable, Any>): SafeVariable {
        return SafeVariable(TransPattern.transAny(exp.value))
    }

    override fun visitSet(exp: SetExp<SafeVariable>): SafeVariable {
        return e.scope.varRef(e.set.union(exp.content.map { visit(it) }))
    }

    override fun visitCase(exp: Case<SafeVariable>): SafeVariable {
        val input = visit(exp.exp)
        val alts = exp.alts

        when (alts.size) {
            0 -> throw InvalidExpressionException("Empty cases are disallowed")
            1 -> return transAlt(e, alts[0], input)
        }

        val safety = patternMatchBodies(exp, input)
        val out = e.tseitin.new()
        alts.zip(safety) { alt, safe ->
            val altT = e.scope.withSafety(safe.toSet()) {
                transAlt(e, alt, input)
            }
            e.tseitin.add(out, SafeVariable(altT.term, altT.safety + safe))
        }
        return e.tseitin.eval(e.scope.varRef(out), e.newVar())
    }

    fun patternMatchBodies(exp: Case<SafeVariable>, input: SafeVariable): List<List<Body>> {
        return exp.alts.mapIndexed { index, alt ->
            val pat = alt.pattern.map { e.newVar() }
            val inp = input.term
            val match = if (pat.isTrivial()) null else TransPattern.patternMatch(pat, inp)?.let { LitBody(it) }
            if (pat.isGround()) {
                listOfNotNull(match) + input.safety
            } else {
                val nomatch = exp.alts.subList(0, index).map { TransPattern.patternNoMatch(it.pattern.map { e.newVar() }, input.term) }
                nomatch + listOfNotNull(match) + input.safety
            }
        }
    }

    fun visitBinds(binds: List<Bind<SafeVariable>>): List<SafeVariable> {
        if (binds.all { it.exp.unscope.freeVars().all { it is FreeVar } }) {
            return binds.map { visit(it.exp.emptyInstantiate()) }
        }
        // Invent new tseitin for each bound variable grouped by binding
        val tseitinName = binds.map { e.tseitin.new() }
        val tseitinRef = tseitinName.map { e.tseitin.eval(e.scope.varRef(it), e.newVar()) }
        // Instantiate bound variables withSafety the new sets
        binds.zip(tseitinName) { (_, exp), tseitin ->
            val meaning = visit(exp.instantiate { V(tseitinRef[it.b]) })
            e.tseitin.add(tseitin, meaning)
        }
        return tseitinRef
    }

    fun setTrans(): TransSet {
        return TransSet(c, e)
    }

    fun boolTrans(): TransBool {
        return TransBool(c, e)
    }
}

fun ExpVisitor<SafeVariable, SafeVariable>.transAlt(e: Environment, alt: Alt<SafeVariable>, input: SafeVariable): SafeVariable {
    if (alt.pattern.isTrivial()) {
        return visit(alt.exp.instantiate { V(input) })
    }

    val pattern = alt.pattern.map { e.newVar() }
    val output = alt.exp
    val vars = pattern.varList()
    val matchBody = LitBody(TransPattern.patternMatch(pattern, input.term)!!)
    return e.scope.with(vars, input, matchBody) { visit(output.instantiate { V(SafeVariable(vars[it.b], input.safety + matchBody)) }) }
}