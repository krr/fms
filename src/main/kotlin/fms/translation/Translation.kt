package fms.translation

import fms.asp.Body
import fms.asp.ByString
import fms.asp.ClingoResult
import fms.asp.Constraint
import fms.asp.Head
import fms.asp.Herbrand
import fms.asp.Rule
import fms.asp.Sign
import fms.asp.Statement
import fms.asp.Term
import fms.asp.Variable
import fms.asp.runClingo
import fms.fml.type.Type
import toStream
import java.io.File
import java.io.PrintWriter
import kotlin.concurrent.thread

class Translation(val command: IDPCommand) {

    var result: SafeVariable? = null
    val outInfo = mutableMapOf<String, Type>()

    val topConstraint = (if (!command.trace.constrain) listOf() else listOf(Constraint(listOf(
            Body.literal(Sign.Negative, "bool", listOf(Variable("X"), Term.tuple())),
            Body.literal("result", listOf(Variable("X"))))))) + listOf(
            Rule(Head.literal("bool", listOf(Herbrand("true"), Term.tuple())), listOf()))

    val scripts = command.scripts.map { ByString(it) }

    private val aspProg: MutableList<Statement> = mutableListOf<Statement>().apply { addAll(scripts); addAll(topConstraint) }

    fun add(stat: Statement) {
        aspProg.add(stat)
    }

    fun writeProgram(ps: PrintWriter) {
        aspProg.forEach { it.toStream(ps); ps.println() }
        assert(result != null)
        val resultRule = Rule(Head.literal("result", listOf(result!!.term)), result!!.safety.toList())
        resultRule.toStream(ps)
        ps.println()
    }

    fun writeToFile(): File {
        val file = File.createTempFile("idp2asp", "tmp")
        val writer = PrintWriter(file)
        writeProgram(writer)
        writer.flush()
        return file
    }

    fun run(): Process {
        return runClingo(this.writeToFile(), command.nbModels, command.trace.stats)
    }

    fun executeRaw(psOut: PrintWriter, psErr: PrintWriter) {
        val process = run()
        thread {
            process.inputStream.reader().copyTo(psOut, 1)
        }
        thread {
            process.errorStream.reader().copyTo(psErr, 1)
        }
        process.waitFor()
    }

    fun execute(): ClingoResult {
        val process = run()
        val outp = ClingoResult.fromJSON(process.inputStream)
        process.waitFor()
        outp.errorStream += process.errorStream.reader().readText()
        outp.exitCode = process.exitValue()
        if (command.trace.printDetail > 0) {
            return outp
        }
        return outp.unMeta(outInfo)
    }
}