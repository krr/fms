package fms.translation

import fms.globalLog
import fms.fml.expression.Expression
import fms.fml.expression.HerbrandExp
import fms.fml.type.InferType
import fms.optimiser.OptiSettings
import fms.optimiser.optimise
import fms.terminal.FreeVarsException

data class TraceOptions(
    var constrain: Boolean,
    var printDetail: Int,
    var printASP: Boolean,
    var stats: Boolean,
    var checkType: Boolean,
    var runClingo: Boolean,
    var printDefault: Boolean
) {
    companion object {
        val nothing: TraceOptions
            get() = TraceOptions(true, 0, false, false, true, true, true)
    }
}

class IDPCommand(
    var nbModels: Int = 0,
    expression: Expression = HerbrandExp("true"),
    var scripts: MutableList<String> = mutableListOf(),
    var trace: TraceOptions = TraceOptions.nothing,
    var opt: OptiSettings = OptiSettings.all,
    var stdLib: Boolean = true
) {
    var expression: Expression = expression
        set(value) {
            if (!value.freeVars().isEmpty()) {
                throw FreeVarsException(value.freeVars())
            }
            if (trace.checkType) {
                InferType.boolCheck(value)
            }
            field = value
        }

    fun translate(): Translation {
        expression = optimise(expression, opt)
        globalLog.debug { "Expression after optimisations: $expression" }

        val trans = Translator(this)
        val top = trans.visit(this.expression.cast())
        return trans.e.aspProg.apply { result = top }
    }
}
