package fms.translation

import fms.asp.Term
import fms.fml.expression.Application
import fms.fml.expression.Case
import fms.fml.expression.Exp
import fms.fml.expression.LangBuiltin
import fms.fml.expression.visitor.ExpVisitor

class TransBool(val c: IDPCommand, val e: Environment = Environment(c)) : ExpVisitor<SafeVariable, SafeVariable> {

    override fun default(exp: Exp<SafeVariable>): SafeVariable {
        return e.bool.eval(Translator(c, e).visit(exp), Term.tuple())
    }

    override fun visitCase(exp: Case<SafeVariable>): SafeVariable {
        if (exp.alts.size == 1) {
            return transAlt(e, exp.alts[0], Translator(c, e).visit(exp.exp))
        }
        return default(exp)
    }

    override fun visitApplication(exp: Application<SafeVariable>): SafeVariable {
        val (f, args) = exp.collectArgs()
        if (f is LangBuiltin) {
            return f.builtin.translateBool(args, Translator(c, e))
        }
        return default(exp)
    }
}