package fms.translation

import fms.asp.Body
import fms.asp.Term
import fms.asp.Variable
import fms.streamToString
import toStream

data class SafeVariable(val term: Term, val safety: Set<Body> = setOf()) {
    override fun toString(): String {
        return "(" + term::toStream.streamToString() + "," + safety.map { it::toStream.streamToString() } + ")"
    }

    fun combine(other: SafeVariable, combineFunc: (Term, Term) -> Term): SafeVariable {
        return SafeVariable(combineFunc(term, other.term), safety + other.safety)
    }

    fun freeVars(): List<Variable> {
        return term.freeVars() + safety.flatMap { it.freeVars() }
    }
}
