package fms.translation

import fms.asp.Body
import fms.asp.Herbrand
import fms.asp.Term
import fms.asp.Variable
import fms.concat
import fms.globalLog

sealed class VarKnowledge(val extraKnowledge: MutableList<Body>)
class ScopedVar(val sourceBody: List<Body>, extraKnowledge: MutableList<Body>) : VarKnowledge(extraKnowledge)
class DerivedVar(val derivedFrom: SafeVariable, val definition: Body, extraKnowledge: MutableList<Body>) : VarKnowledge(extraKnowledge)

@Suppress("UNCHECKED_CAST")
class ScopeManager {

    private var vars: MutableMap<Variable, VarKnowledge> = mutableMapOf()
    val scopedVars: Map<Variable, ScopedVar>
        get() = vars.filter { it.value is ScopedVar } as Map<Variable, ScopedVar>
    val derivedVars: Map<Variable, DerivedVar>
        get() = vars.filter { it.value is DerivedVar } as Map<Variable, DerivedVar>

    fun tuple(): Herbrand = Term.tuple(scopedVars.map { it.key })
    fun safety() = scopedVars.flatMap { it.value.extraKnowledge + it.value.sourceBody }.toSet()

    /**
     * Creates a setRef withSafety the current variables in scope
     */
    fun varRef(name: Herbrand): SafeVariable =
            SafeVariable(tupRef(name), safety())

    fun tupRef(name: Herbrand) =
            Term.tuple(name, tuple())

    /**
     * Filter Scoped Variables to the ones referred to in the List of SafeVariables
     */
    fun <A> filter(freeVars: List<SafeVariable>, f: () -> A): A {
        val relevant = freeVars.flatMap {
            it.freeVars().mapNotNull {
                val v = vars[it]
                when (v) {
                // TODO: take transitive closure for derivedVars
                    is DerivedVar -> v.derivedFrom.freeVars()
                    is ScopedVar -> listOf(it)
                    else -> null
                }
            }
        }.concat()

        val bup = vars
        vars = vars.filterTo(mutableMapOf()) { relevant.contains(it.key) || it.value is DerivedVar }
        if (bup.size != vars.size) {
            globalLog.trace { "Filtered $bup to $vars" }
        }
        val out = f()
        vars = bup
        return out
    }

    /*
     * Add these bodies to all variables who are referred to
     */
    fun <A> withSafety(s: Set<Body>, f: () -> A): A {
        val freeVars = s.flatMap { it.freeVars() }
        freeVars.forEach {
            vars[it]?.apply { extraKnowledge += s }
        }
        val out = f()
        freeVars.forEach {
            vars[it]?.apply { extraKnowledge -= s }
        }
        return out
    }

    fun <A> with(s: Variable, generatingSet: Set<Body>, f: () -> A): A {
        vars[s] = ScopedVar(generatingSet.toList(), mutableListOf())
        val out = f()
        vars.remove(s)
        return out
    }

    fun <A> with(s: List<Pair<Variable, Set<Body>>>, f: () -> A): A {
        s.forEach {
            vars[it.first] = ScopedVar(it.second.toList(), mutableListOf())
        }
        val out = f()
        s.forEach { vars.remove(it.first) }
        return out
    }

    fun <A> with(s: List<Variable>, dependsOn: SafeVariable, definition: Body, f: () -> A): A {
        s.map {
            vars[it] = DerivedVar(dependsOn, definition, mutableListOf())
        }
        val out = f()
        s.map { vars.remove(it) }
        return out
    }
}
