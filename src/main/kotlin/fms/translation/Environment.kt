package fms.translation

import fms.asp.Body
import fms.asp.ChoiceElement
import fms.asp.ChoiceHead
import fms.asp.ClassLit
import fms.asp.ClassNaf
import fms.asp.Head
import fms.asp.LitBody
import fms.asp.Rule
import fms.asp.Sign
import fms.asp.StringTerm
import fms.asp.Term
import fms.asp.Variable
import fms.fml.type.NewTypeProvider
import fms.fml.type.TypeRef

class Environment(command: IDPCommand) {

    companion object {
        val lamInter = "lamInter"
    }

    val tseitin = OutConcept("inter", "ts", this)
    val set = OutConcept("member", "s", this)
    val lambda = OutConcept("lamDom", "l", this)
    val bool = OutConcept("bool", "b", this)
    val print = OutConcept("out", "p", this)
    val scope = ScopeManager()

    private var varID: Int = 0
    val aspProg = Translation(command)

    fun newVar(): Variable = Variable("X" + (varID++).toString())

    fun addMeaning(lam: SafeVariable, input: Term, output: SafeVariable) {
        val head = Head.literal(lamInter, listOf(lam.term, input, output.term))
        val body = lam.safety + output.safety + lambda.eval(lam, input).safety
        val rule = Rule(head, body.toList())
        aspProg.add(rule)
    }

    fun applyLam(lam: SafeVariable, input: SafeVariable, extraDomain: Boolean = true): SafeVariable {
        if (extraDomain) {
            lambda.add(lam, input)
        }
        val out = newVar()
        val safety = setOf(Body.literal(lamInter, listOf(lam.term, input.term, out))) + lam.safety + input.safety
        return SafeVariable(out, safety)
    }

    fun asBool(safety: Set<Body>): SafeVariable {
        val herb = bool.union(SafeVariable(Term.tuple(), safety))
        return scope.varRef(herb)
    }

    fun subset(member: SafeVariable, underbound: Int? = null, upperbound: Int? = null): SafeVariable {
        val subsetPred = set.new()

        val varr = newVar()
        val classLit = set.classLit(listOf(scope.tupRef(subsetPred), varr))
        val generClass = set.classLit(listOf(member.term, varr))
        val gener = ClassNaf(Sign.Positive, generClass)

        val choiceHead = ChoiceHead(listOf(ChoiceElement(classLit, listOf(LitBody(gener)))), underbound, upperbound)
        aspProg.add(Rule(choiceHead, member.safety.toList()))
        return scope.varRef(subsetPred)
    }

    fun function(output: SafeVariable, isPartial: Boolean = false): SafeVariable {
        val lam = scope.varRef(lambda.new())
        val varr = newVar()

        val classLit = ClassLit(Sign.Positive, lamInter, listOf(lam.term, varr, output.term))
        val choiceHead = ChoiceHead(listOf(ChoiceElement(classLit, output.safety.toList())), if (isPartial) 0 else 1, 1)

        val rule = Rule(choiceHead, lambda.eval(lam, varr).safety.toList())
        aspProg.add(rule)
        return lam
    }

    fun output(name: String, trans: SafeVariable, typeRef: TypeRef) {
        aspProg.outInfo[name] = typeRef.type(NewTypeProvider())
        print.add(SafeVariable(StringTerm(name)), trans)
    }

    fun makeTseitin(toTseitin: SafeVariable): SafeVariable {
        val ts = tseitin.union(toTseitin)
        return tseitin.eval(scope.varRef(ts), newVar())
    }
}
