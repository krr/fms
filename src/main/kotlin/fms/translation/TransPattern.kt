package fms.translation

import fms.asp.AggBody
import fms.asp.AggElement
import fms.asp.Aggregate
import fms.asp.AggregateFunction
import fms.asp.AggregateLit
import fms.asp.Binop
import fms.asp.Body
import fms.asp.BuiltinNaf
import fms.asp.Herbrand
import fms.asp.IntTerm
import fms.asp.LitBody
import fms.asp.NafLit
import fms.asp.StringTerm
import fms.asp.Term
import fms.asp.Variable
import fms.fml.expression.APattern
import fms.fml.expression.AnonymVar
import fms.fml.expression.Comparison
import fms.fml.expression.Constructor
import fms.fml.expression.Injected
import fms.fml.expression.VarPattern

object TransPattern {
    fun patternMatch(pattern: APattern<Variable>, term: Term): NafLit? {
        return when (pattern) {
            is VarPattern -> BuiltinNaf(pattern.name, Binop.EQ, term)
            is Constructor -> BuiltinNaf(transConstrPattern(pattern), Binop.EQ, term)
            is Injected<*, *> -> BuiltinNaf(transAny(pattern.obj ?: throw Error("Can't inject null")), Binop.EQ, term)
            is AnonymVar -> null
            is Comparison -> pattern.pred(term)
        }
    }

    fun patternNoMatch(pattern: APattern<Variable>, term: Term): Body {
        when (pattern) {
            is Injected<*, *> -> return LitBody(BuiltinNaf(transAny(pattern.obj
                    ?: throw Error("Can't inject null")), Binop.NEQ, term))
        }
        val match = patternMatch(pattern, term)
        val aggregate = Aggregate(AggregateFunction.AggCount, listOf(AggElement(listOf(), listOfNotNull(match).map(::LitBody))))
        val aggLit = AggregateLit(aggregate, Binop.EQ, IntTerm(0))
        val aggBody = AggBody(false, aggLit)
        return aggBody
    }

    fun transConstrPattern(pattern: APattern<Variable>): Term {
        return when (pattern) {
            is Constructor -> Herbrand(pattern.name, pattern.args.map(this::transConstrPattern))
            is VarPattern -> pattern.name
            is Injected<*, *> -> transAny(pattern.obj ?: throw Error("Can't inject null"))
            is AnonymVar -> fms.asp.AnonymVar
            is Comparison -> TODO("Can't do comparisons in constructor patterns")
        }
    }

    fun transAny(value: Any): Term {
        return when (value) {
            is Int -> IntTerm(value)
            is String -> StringTerm(value)
            else -> throw InvalidExpressionException("Only Int and String injections can be translated")
        }
    }
}
