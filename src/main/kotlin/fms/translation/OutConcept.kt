package fms.translation

import fms.asp.Body
import fms.asp.ClassLit
import fms.asp.Head
import fms.asp.Herbrand
import fms.asp.Rule
import fms.asp.Sign
import fms.asp.Term

class OutConcept(val predName: String, val prefix: String, val e: Environment) {
    private var idGen: Int = 0

    fun new(): Herbrand =
            Herbrand(prefix + (idGen++).toString())

    fun eval(ref: SafeVariable, t: Term): SafeVariable =
            SafeVariable(t, setOf(Body.literal(predName, listOf(ref.term, t))) + ref.safety)

    fun notEval(ref: SafeVariable, t: Term): SafeVariable =
            SafeVariable(t, setOf(Body.literal(Sign.Negative, predName, listOf(ref.term, t))) + ref.safety)

    fun classLit(args: List<Term>, sign: Sign = Sign.Positive): ClassLit =
            ClassLit(sign, predName, args)

    fun add(name: Herbrand, content: SafeVariable) {
        add(e.scope.varRef(name), content)
    }

    fun add(name: SafeVariable, content: SafeVariable) {
        val head = Head.literal(predName, listOf(name.term, content.term))
        val body = content.safety + e.scope.safety() + name.safety
        val rule = Rule(head, body.toList())
        e.aspProg.add(rule)
    }

    fun union(vars: Iterable<SafeVariable>): Herbrand {
        val name = new()
        vars.forEach { add(name, it) }
        return name
    }

    fun union(vararg vars: SafeVariable): Herbrand =
            union(vars.toList())
}
