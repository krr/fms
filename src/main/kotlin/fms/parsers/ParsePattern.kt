package fms.parsers

import fms.asp.Binop
import fms.asp.BuiltinNaf
import fms.asp.IntTerm
import fms.asp.NafLit
import fms.asp.Term
import fms.fml.expression.AnonymVar
import fms.fml.expression.Comparison
import fms.fml.expression.Constructor
import fms.fml.expression.Injected
import fms.fml.expression.Pattern
import fms.fml.expression.VarPattern

object ParsePattern : FOIDPParserBaseVisitor<Pattern>() {

    override fun visitParenPattern(ctx: FOIDPParser.ParenPatternContext): Pattern {
        return visit(ctx.pattern())
    }

    override fun visitVarPattern(ctx: FOIDPParser.VarPatternContext): Pattern =
            VarPattern(ctx.text)

    override fun visitConstructedPattern(ctx: FOIDPParser.ConstructedPatternContext): Pattern =
            Constructor(ctx.ID().text, ctx.pattern().map { visit(it) })

    override fun visitIntLitPattern(ctx: FOIDPParser.IntLitPatternContext): Pattern =
            Injected(Integer.parseInt(ctx.INTLIT().text))

    override fun visitStringLitPattern(ctx: FOIDPParser.StringLitPatternContext): Pattern =
            Injected(ctx.STRING().text.removeSurrounding("\""))

    override fun visitAnonymPattern(ctx: FOIDPParser.AnonymPatternContext): Pattern =
            AnonymVar()

    override fun visitIntCompPattern(ctx: FOIDPParser.IntCompPatternContext): Pattern =
            Comparison({ t -> builtinMap[ctx.op.text]!!(t, IntTerm(Integer.parseInt(ctx.INTLIT().text))) })

    private val builtinMap: Map<String, (Term, Term) -> NafLit> = mapOf(
            Pair("<", { a, b -> BuiltinNaf(a, Binop.LT, b) }),
            Pair("=<", { a, b -> BuiltinNaf(a, Binop.LEQ, b) }),
            Pair(">", { a, b -> BuiltinNaf(a, Binop.GT, b) }),
            Pair(">=", { a, b -> BuiltinNaf(a, Binop.GEQ, b) }),
            Pair("=", { a, b -> BuiltinNaf(a, Binop.EQ, b) }),
            Pair("~=", { a, b -> BuiltinNaf(a, Binop.NEQ, b) })
    )

    override fun visitTuplePattern(ctx: FOIDPParser.TuplePatternContext): Pattern =
            Constructor("", ctx.pattern().map { visit(it) })

    override fun visitUnitPattern(ctx: FOIDPParser.UnitPatternContext): Pattern =
            Constructor("", listOf())

    override fun defaultResult(): Pattern {
        throw Exception("No Default Parse Binding")
    }
}
