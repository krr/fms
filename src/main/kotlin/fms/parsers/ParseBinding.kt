package fms.parsers

import fms.fml.builtin.Ite
import fms.fml.expression.Alt
import fms.fml.expression.Application
import fms.fml.expression.BindPragma
import fms.fml.expression.Binding
import fms.fml.expression.Case
import fms.fml.expression.Constructor
import fms.fml.expression.Expression
import fms.fml.expression.HerbrandExp
import fms.fml.expression.Lambda
import fms.fml.expression.LangBuiltin
import fms.fml.expression.Pattern
import fms.fml.expression.SetExp
import fms.fml.expression.V
import fms.fml.expression.VarPattern
import fms.fml.type.TypeEnvironment
import fms.translation.InvalidExpressionException
import org.antlr.v4.runtime.ParserRuleContext
import java.nio.file.Paths

object ParseBinding : FOIDPParserBaseVisitor<List<Binding>>() {

    override fun visitAssignBind(ctx: FOIDPParser.AssignBindContext): List<Binding> {
        return visit(ctx.assignment())
    }

    override fun visitDeclBind(ctx: FOIDPParser.DeclBindContext): List<Binding> {
        return visit(ctx.declaration())
    }

    override fun visitFuncAssignment(ctx: FOIDPParser.FuncAssignmentContext): List<Binding> =
            listOf(Binding(VarPattern(ctx.ID().text), ctx.pattern().foldRight(ParseExp.visit(ctx.expr()),
                    { arg, e -> Lambda.make(ParsePattern.visit(arg), e) }), null, BindPragma()))

    override fun visitPatternAssignment(ctx: FOIDPParser.PatternAssignmentContext): List<Binding> =
            listOf(Binding(ParsePattern.visit(ctx.pattern()), ParseExp.visit(ctx.expr()), null, BindPragma()))

    override fun visitOpenDecl(ctx: FOIDPParser.OpenDeclContext): List<Binding> =
            listOf(Binding(VarPattern(ctx.ID().text), ParseDomain.visit(ctx.domain()), null, BindPragma(inline = false)))

    fun makeConstr(name: String, arity: Int, set: Boolean = false): Expression {
        val args = (1..arity).map { V("arg" + it.toString()) }
        val applied = HerbrandExp(name, args).let {
            if (set) {
                SetExp(listOf(it))
            } else {
                it
            }
        }
        val output = args.foldRight<V<String>, Expression>(applied) { v, exp -> Lambda.make(VarPattern(v.a), exp) }
        return output
    }

    override fun visitStrictAssignStat(ctx: FOIDPParser.StrictAssignStatContext): List<Binding> {
        return listOf(Binding(VarPattern(ctx.ID().text), ParseExp.visit(ctx.expr()), null, BindPragma(print = true, strict = true)))
    }

    override fun visitConstrDecl(ctx: FOIDPParser.ConstrDeclContext): List<Binding> {
        val name = ctx.ID().text
        val arity = Integer.parseInt(ctx.INTLIT().text)
        return listOf(Binding(VarPattern(ctx.ID().text), makeConstr(name, arity), null, BindPragma(inline = true)))
    }

    override fun visitExternDecl(ctx: FOIDPParser.ExternDeclContext): List<Binding> {
        val name = "@" + ctx.ID().text
        val type = ParseType.visit(ctx.type())
        val arity = type.arity
        var output = makeConstr(name, arity, type.returnsSet())
        return listOf(Binding(VarPattern(ctx.ID().text), output, type.generalize(TypeEnvironment.empty), BindPragma()))
    }

    override fun visitLibraryImport(ctx: FOIDPParser.LibraryImportContext): List<Binding> {
        val text = ctx.STRING().text
        val parsed = text.substring(1, text.length - 1)
        return parseLib(parsed)
    }

    fun parseLib(name: String): List<Binding> {
        val res = FOIDPParser::class.java.getResource("/" + name)
        if (res != null) {
            return ParseBinding.visit(parseStream(res.openStream()).foidpLib())
        }
        val file = Paths.get(name).toFile()
        if (file.exists()) {
            return ParseBinding.visit(parseStream(file.inputStream()).foidpLib())
        }
        val file2 = Paths.get(System.getProperty("user.dir"), name).toFile()
        if (file2.exists()) {
            return ParseBinding.visit(parseStream(file.inputStream()).foidpLib())
        }
        throw InvalidExpressionException("Invalid import of library file: $name")
    }

    fun ParserRuleContext.symbol(): String? {
        return when (this) {
            is FOIDPParser.PatternAssignmentContext -> null
            is FOIDPParser.TypedAssignmentContext -> this.name.text
            is FOIDPParser.FuncAssignmentContext -> this.name.text
            is FOIDPParser.GuardAssignmentContext -> this.name.text
            is FOIDPParser.PragmaAssignmentContext -> this.name.text
            is FOIDPParser.ConstrDeclContext -> this.ID().text
            is FOIDPParser.ExternDeclContext -> this.ID().text
            is FOIDPParser.OpenDeclContext -> this.ID().text
            is FOIDPParser.StrictAssignStatContext -> this.name.text
            else -> {
                throw Exception("Can't get symbol of ${this::class}")
            }
        }
    }

    sealed class PBind(open val b: ParserRuleContext) {
        companion object {
            fun make(b: FOIDPParser.BindContext): PBind {
                if (b is FOIDPParser.AssignBindContext) {
                    return PAssignBind(b.assignment())
                }
                if (b is FOIDPParser.DeclBindContext) {
                    return PDeclBind(b.declaration())
                }
                throw Error()
            }
        }

        class PAssignBind(override val b: FOIDPParser.AssignmentContext) : PBind(b)
        class PDeclBind(override val b: FOIDPParser.DeclarationContext) : PBind(b)
    }

    @Suppress("UNCHECKED_CAST")
    fun parseAssignments(assignment: List<PBind>): List<Binding> {
        if (assignment.isEmpty()) {
            return listOf()
        }
        val first = assignment.first()
        if (first.b.symbol() == null) {
            return visit(first.b) + parseAssignments(assignment.drop(1))
        }
        val part = assignment.takeWhile { it.b.symbol() != null && it.b.symbol() == first.b.symbol() }
        val rest = assignment.drop(part.size)
        return parseAssignmentBlock(part) + parseAssignments(rest)
    }

    private fun parseAssignmentBlock(part: List<PBind>): List<Binding> {
        val first = part[0].b
        if (first is FOIDPParser.TypedAssignmentContext) {
            val curTrans = parseAssignmentBlock(part.drop(1))
            val t = ParseType.visit(first.type())
            return listOf(curTrans[0].copy(type = t.generalize(TypeEnvironment.empty))) + curTrans.drop(1)
        }
        if (first is FOIDPParser.PragmaAssignmentContext) {
            val curTrans = parseAssignmentBlock(part.drop(1))
            val extraPragma: (BindPragma) -> Unit = when (first.ID(1).text) {
                "inline" -> { b -> b.inline = true }
                "noinline" -> { b -> b.inline = false }
                "print" -> { b -> b.print = true }
                "noprint" -> { b -> b.print = false }
                else -> throw Exception("Invalid Pragma: ${first.ID(1).text}")
            }
            return curTrans.apply { this.forEach { it.pragma.also(extraPragma) } }
        }

        val bnds = part.map {
            val b = it.b
            when (b) {
                is FOIDPParser.FuncAssignmentContext -> {
                    val pats = b.pattern().map { ParsePattern.visit(it) }
                    val exp = ParseExp.visit(b.expr())
                    return@map pats to exp
                }
                is FOIDPParser.GuardAssignmentContext -> {
                    val pats = b.pattern().map { ParsePattern.visit(it) }
                    val exp = b.guardExp().foldRight(HerbrandExp<String>("emptyElse") as Expression, this::parseGuard)
                    return@map pats to exp
                }
                else -> {
                    return visit(b)
                }
            }
        }
        return listOf(parseGroupBinding(first.symbol()!!, bnds))
    }

    fun parseGuard(g: FOIDPParser.GuardExpContext, exp: Expression): Expression {
        val cond = ParseExp.visit(g.cond)
        val then = ParseExp.visit(g.`val`)
        return Application.make(LangBuiltin(Ite), cond, then, exp)
    }

    fun parseGroupBinding(name: String, ctnt: List<Pair<List<Pattern>, Expression>>): Binding {
        if (ctnt.isEmpty()) {
            throw Exception("Can't parse empty group")
        }
        if (ctnt.size == 1) {
            val f = ctnt.first()
            return Binding(VarPattern(name), f.first.foldRight(f.second) { a, b -> Lambda.make(a, b) }, null, BindPragma())
        }
        val rule = ctnt[0]
        val vars = ('a' until ('a' + rule.first.size)).map { it.toString() }
        val varTup = HerbrandExp.tuple(vars.map { V(it) })
        val alts = ctnt.map {
            Alt.make(Constructor.tuple(it.first), it.second)
        }
        val lambda = vars.foldRight(Case(varTup, alts) as Expression) { a, b -> Lambda.make(a, b) }
        // TODO: Assert all ID's same
        val out = Binding(VarPattern(name), lambda, null, BindPragma())
        return out
    }

    override fun visitFoidpLib(ctx: FOIDPParser.FoidpLibContext): List<Binding> =
            parseAssignments(ctx.assignment().map(PBind::PAssignBind)) +
                    ctx.libraryImport().flatMap(this::visit)

    override fun defaultResult(): List<Binding> {
        throw Exception("No Default Parse Binding")
    }
}