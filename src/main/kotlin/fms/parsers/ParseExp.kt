package fms.parsers

import fms.fml.builtin.Ite
import fms.fml.builtin.Range
import fms.fml.builtin.UnaryMinusB
import fms.fml.expression.Alt
import fms.fml.expression.Application
import fms.fml.expression.Case
import fms.fml.expression.Expression
import fms.fml.expression.HerbrandExp
import fms.fml.expression.Injection
import fms.fml.expression.Lambda
import fms.fml.expression.LangBuiltin
import fms.fml.expression.Let
import fms.fml.expression.V
import fms.fml.type.TypeEnvironment
import org.antlr.v4.runtime.ParserRuleContext
import org.antlr.v4.runtime.tree.ParseTree

object ParseExp : FOIDPParserBaseVisitor<Expression>() {

    override fun visitDollarApplication(ctx: FOIDPParser.DollarApplicationContext): Expression =
        Application.make(visit(ctx.expr(0)), visit(ctx.expr(1)))

    override fun visitNonDollar(ctx: FOIDPParser.NonDollarContext) =
            visit(ctx.expr1())

    override fun visitExprStat(ctx: FOIDPParser.ExprStatContext): Expression =
            visit(ctx.expr())

    override fun visitAbstraction(ctx: FOIDPParser.AbstractionContext): Expression =
            ctx.pattern().foldRight(visit(ctx.expr())) { p, e -> Lambda.make(ParsePattern.visit(p), e) }

    override fun visitApplication(ctx: FOIDPParser.ApplicationContext): Expression =
            Application.make(visit(ctx.expr3(0)), visit(ctx.expr3(1)))

    override fun visitId(ctx: FOIDPParser.IdContext): Expression =
            V(ctx.text)

    override fun visitIntLit(ctx: FOIDPParser.IntLitContext): Expression {
        val int = Integer.parseInt(ctx.INTLIT().text)
        return Injection<String, Int>(int)
    }

    override fun visitStringLit(ctx: FOIDPParser.StringLitContext): Expression =
            Injection<String, String>(ctx.STRING().text.removeSurrounding("\""))

    override fun visitLet(ctx: FOIDPParser.LetContext): Expression {
        val expr = visit(ctx.expr())
        val assignments = ctx.bindings().assignment().map(ParseBinding.PBind::PAssignBind)
        val binds = ParseBinding.parseAssignments(assignments)
        return Let.make(binds, expr)
    }

    override fun visitCaseExpr(ctx: FOIDPParser.CaseExprContext): Expression {
        val expr = visit(ctx.expr())
        val alts = ctx.alt().map { parseAlt(it) }
        return Case(expr, alts)
    }

    fun parseAlt(ctx: FOIDPParser.AltContext): Alt<String> {
        return Alt.make(ParsePattern.visit(ctx.pattern()), visit(ctx.expr()))
    }

    override fun visitNotExpr(ctx: FOIDPParser.NotExprContext): Expression =
            Application(ParseBuiltin.builtin(ctx.NOT().text), visit(ctx.expr2()))

    override fun visitOpExpr(ctx: FOIDPParser.OpExprContext) =
            ParseBuiltin.binaryOperator(ctx.op, ctx.expr1())

    override fun visitSetExpr(ctx: FOIDPParser.SetExprContext): Expression =
            visit(ctx.set())

    override fun visitSet(ctx: FOIDPParser.SetContext): Expression {
        return ParseCommand.visit(ctx)
    }

    override fun visitIteExpr(ctx: FOIDPParser.IteExprContext): Expression =
            Application.make(LangBuiltin(Ite), ctx.expr().map { visit(it) })

    override fun visitBuiltinExpr(ctx: FOIDPParser.BuiltinExprContext): Expression =
            ParseBuiltin.builtin(ctx.text)

    override fun visitUnitExpression(ctx: FOIDPParser.UnitExpressionContext): Expression =
            HerbrandExp<String>("", listOf())

    override fun visitTupleExpression(ctx: FOIDPParser.TupleExpressionContext) =
            HerbrandExp<String>("", ctx.expr().map { visit(it) })

    override fun visitRangeExpr(ctx: FOIDPParser.RangeExprContext): Expression =
            Application.make(LangBuiltin(Range), ctx.expr().map { visit(it) })

    override fun visitParenExpression(ctx: FOIDPParser.ParenExpressionContext): Expression =
            visit(ctx.expr())

    override fun visitInfixExpr(ctx: FOIDPParser.InfixExprContext): Expression =
            Application.make(V(ctx.ID().text), ctx.expr1().map { visit(it) })

    override fun visitTypedExpr(ctx: FOIDPParser.TypedExprContext): Expression {
        return visit(ctx.expr1()).apply { typeRef.type = ParseType.visit(ctx.type()).generalize(TypeEnvironment.empty) }
    }

    override fun visitUnaryMinExpr(ctx: FOIDPParser.UnaryMinExprContext): Expression {
        return Application.make(LangBuiltin(UnaryMinusB), visit(ctx.expr2()))
    }

    override fun defaultResult(): Expression {
        throw Exception("Unparsed rule")
    }

    override fun visit(tree: ParseTree): Expression {
        return super.visit(tree).apply { locate(tree as? ParserRuleContext) }
    }

    override fun visitSimpleExpr(ctx: FOIDPParser.SimpleExprContext): Expression {
        return visit(ctx.expr2())
    }

    override fun visitBaseExpr(ctx: FOIDPParser.BaseExprContext): Expression {
        return visit(ctx.expr3())
    }
}
