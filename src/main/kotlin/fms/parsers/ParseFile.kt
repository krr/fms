package fms.parsers

import fms.fml.builtin.All
import fms.fml.expression.Application
import fms.fml.expression.Binding
import fms.fml.expression.Expression
import fms.fml.expression.HerbrandExp
import fms.fml.expression.LangBuiltin
import fms.fml.expression.Let
import fms.fml.expression.SetExp
import fms.translation.IDPCommand

object ParseFile : FOIDPParserBaseVisitor<IDPCommand>() {

    private fun handleDirectives(ctx: FOIDPParser.FoidpFileContext): IDPCommand {
        val command = IDPCommand()
        (ctx.directive() + ctx.script()).forEach {
            ParseDirective.visit(it).invoke(command)
        }
        return command
    }

    override fun visitFoidpFile(ctx: FOIDPParser.FoidpFileContext): IDPCommand {
        val command = handleDirectives(ctx)

        val stats = ctx.libraryImport().flatMap(ParseBinding::visit).toMutableList()

        if (command.stdLib) {
            stats += ParseBinding.parseLib("stdlib.idp4")
        }

        val (exprs, extraStats) = splitLines(ctx)
        if (command.trace.printDefault) {
            extraStats.forEach(Binding::printedBinding)
        }
        stats += extraStats

        val expression = when (exprs.size) {
            0 -> HerbrandExp("true", listOf())
            else -> {
                Application.make(LangBuiltin(All), SetExp(exprs))
            }
        }
        val out = Let.make(stats, expression).addOperators()

        command.expression = out

        return command
    }

    private fun splitLines(ctx: FOIDPParser.FoidpFileContext): Pair<List<Expression>, List<Binding>> {
        val exprs = mutableListOf<Expression>()
        val statements = ctx.line().map { it.statement() }
        val assignList = mutableListOf<FOIDPParser.BindContext>()

        statements.forEach {
            when (it) {
                is FOIDPParser.ExprStatContext -> exprs.add(ParseExp.visit(it))
                is FOIDPParser.BindStatContext -> assignList.add(it.bind())
            }
        }
        val stats = ParseBinding.parseAssignments(assignList.map { ParseBinding.PBind.make(it) })
        return exprs to stats
    }
}

private fun Binding.printedBinding() {
    if (pragma.inline == true || pragma.print == false) {
        return
    }
    this.apply { pragma.print = true }
}
