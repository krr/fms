package fms.parsers

import fms.fml.expression.Exp
import fms.fml.expression.Let
import fms.fml.type.TypeEnvironment
import org.antlr.v4.runtime.CharStreams
import org.antlr.v4.runtime.CommonTokenStream
import java.io.InputStream

fun lexStream(stream: InputStream) =
        FOIDPLexer(CharStreams.fromStream(stream))

fun parseStream(stream: InputStream): FOIDPParser =
        FOIDPParser(CommonTokenStream(lexStream(stream)))

fun lexString(string: String) =
        FOIDPLexer(CharStreams.fromString(string))

fun parseString(string: String) =
        FOIDPParser(CommonTokenStream(lexString(string)))

fun asExpr(string: String) =
        ParseExp.visit(parseString(string).expr())

fun asType(string: String) =
        ParseType.visit(parseString(string).type())

fun asScheme(string: String) =
        asType(string).generalize(TypeEnvironment.empty)

fun Exp<String>.addLib(string: String) = Let.make(ParseBinding.parseLib(string), this)

fun Exp<String>.addOperators() = this.addLib("operators.idp4")
fun Exp<String>.addStdLib() = this.addLib("stdlib.idp4")