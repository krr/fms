package fms.parsers

import fms.terminal.Execute
import fms.terminal.Ignore
import fms.terminal.TerminalCommand
import fms.terminal.TypeQuery

object ParseTerminalCommand : FOIDPParserBaseVisitor<TerminalCommand>() {

    override fun visitTypeQuery(ctx: FOIDPParser.TypeQueryContext): TerminalCommand {
        return TypeQuery(ParseExp.visit(ctx.expr()))
    }

    override fun visitRunQuery(ctx: FOIDPParser.RunQueryContext): TerminalCommand {
        if (ctx.text.trim() == "<EOF><EOF>") return Ignore
        return Execute(ParseFile.visit(ctx.foidpFile()))
    }

    override fun defaultResult(): TerminalCommand {
        throw Exception("No Default Parse Binding")
    }
}
