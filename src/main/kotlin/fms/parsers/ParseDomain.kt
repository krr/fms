package fms.parsers

import fms.fml.builtin.LamFuncDeclare
import fms.fml.expression.Application
import fms.fml.expression.Expression
import fms.fml.expression.HerbrandExp
import fms.fml.expression.LangBuiltin
import fms.fml.builtin.PredDeclare
import fms.fml.builtin.PropDeclare
import fms.fml.builtin.SubsetDeclare
import fms.fml.builtin.TotalElementDeclare
import fms.fml.builtin.TotalFuncDeclare
import fms.fml.expression.Lambda
import fms.fml.expression.V
import fms.fml.expression.VarPattern

object ParseDomain : FOIDPParserBaseVisitor<Expression>() {
    override fun visitSetDomain(ctx: FOIDPParser.SetDomainContext) =
            Application(LangBuiltin(SubsetDeclare), ParseExp.visit(ctx.expr()))

    override fun visitElemDomain(ctx: FOIDPParser.ElemDomainContext) =
            Application(LangBuiltin(TotalElementDeclare), ParseExp.visit(ctx.expr()))

    override fun visitFuncDomain(ctx: FOIDPParser.FuncDomainContext): Expression {
        return arity(Integer.parseInt(ctx.INTLIT().text), Application.make(LangBuiltin(TotalFuncDeclare), ParseExp.visit(ctx.expr())))
    }

    override fun visitLamFuncDomain(ctx: FOIDPParser.LamFuncDomainContext) =
            arity(Integer.parseInt(ctx.INTLIT().text), Application.make(LangBuiltin(LamFuncDeclare), ParseExp.visit(ctx.expr())))

    override fun visitPredDomain(ctx: FOIDPParser.PredDomainContext): Expression =
            arity(Integer.parseInt(ctx.INTLIT().text), Application.make(LangBuiltin(PredDeclare)))

    override fun visitPropDomain(ctx: FOIDPParser.PropDomainContext): Expression =
            LangBuiltin(PropDeclare)

    fun arity(arity: Int, expr: Expression): Expression {
        if (arity == 1) {
            return expr
        }
        val args = (1..arity).map { V("arg" + it.toString()) }
        val applied = Application.make(expr, HerbrandExp.tuple(args))
        val output = args.foldRight<V<String>, Expression>(applied) { v, exp -> Lambda.make(VarPattern(v.a), exp) }
        return output
    }
}
