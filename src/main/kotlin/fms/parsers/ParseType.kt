package fms.parsers

import fms.fml.type.Apply
import fms.fml.type.BaseType
import fms.fml.type.Const
import fms.fml.type.Type
import fms.fml.type.Type.Companion.Arrow
import fms.fml.type.Var

object ParseType : FOIDPParserBaseVisitor<Type>() {

    override fun visitVarType(ctx: FOIDPParser.VarTypeContext): Type {
        return try {
            Const(BaseType.valueOf(ctx.ID().text))
        } catch (_: IllegalArgumentException) {
            Var(ctx.ID().text)
        }
    }

    override fun visitParenType(ctx: FOIDPParser.ParenTypeContext): Type {
        return visit(ctx.type())
    }

    override fun visitArrowType(ctx: FOIDPParser.ArrowTypeContext): Type {
        return Arrow(visit(ctx.type(0)), visit(ctx.type(1)))
    }

    override fun visitApplicationType(ctx: FOIDPParser.ApplicationTypeContext): Type {
        return Apply(visit(ctx.type(0)), visit(ctx.type(1)))
    }

    override fun visitTupleType(ctx: FOIDPParser.TupleTypeContext): Type =
            ctx.type().fold<FOIDPParser.TypeContext, Type>(Type.Tuple, { a, b -> Apply(a, visit(b)) })

    override fun visitUnitType(ctx: FOIDPParser.UnitTypeContext) =
            Type.Unit

    override fun defaultResult(): Type {
        throw Exception("No Default Parse Binding")
    }
}
