package fms.parsers

import fms.asp.ArithOp
import fms.asp.Binop
import fms.fml.builtin.AbsoluteValue
import fms.fml.builtin.And
import fms.fml.builtin.Count
import fms.fml.builtin.Exists
import fms.fml.builtin.Extract
import fms.fml.builtin.Forall
import fms.fml.builtin.Graph
import fms.fml.builtin.MaxBuiltin
import fms.fml.builtin.Maximize
import fms.fml.builtin.Member
import fms.fml.builtin.MinBuiltin
import fms.fml.builtin.Minimize
import fms.fml.builtin.Not
import fms.fml.builtin.Or
import fms.fml.builtin.SetBind
import fms.fml.builtin.SumBuiltin
import fms.fml.builtin.SumBy
import fms.fml.builtin.TrueBuiltin
import fms.fml.expression.Application
import fms.fml.expression.Exp
import fms.fml.expression.Expression
import fms.fml.expression.LangBuiltin
import fms.fml.expression.V
import fms.optimiser.ExpressionTransformer
import org.antlr.v4.runtime.Token

object ParseBuiltin {

    private val builtinMap: Map<String, Exp<String>> = mapOf(
            Pair("+", LangBuiltin<String>(ArithOp.ArPlus)),
            Pair("-", LangBuiltin(ArithOp.ArMinus)),
            Pair("*", LangBuiltin(ArithOp.ArTimes)),
            Pair("/", LangBuiltin(ArithOp.ArDiv)),
            Pair("%", LangBuiltin(ArithOp.ArMod)),
            Pair("\$pow", LangBuiltin(ArithOp.ArPow)),
            Pair("<", LangBuiltin(Binop.LT)),
            Pair("=<", LangBuiltin(Binop.LEQ)),
            Pair("=", LangBuiltin(Binop.EQ)),
            Pair("~=", LangBuiltin(Binop.NEQ)),
            Pair(">", LangBuiltin(Binop.GT)),
            Pair(">=", LangBuiltin(Binop.GEQ)),
            Pair("|", LangBuiltin(Or)),
            Pair("&", LangBuiltin(And)),
            Pair("!", LangBuiltin(Forall)),
            Pair("?", LangBuiltin(Exists)),
            Pair("~", LangBuiltin(Not)),
            Pair("<|", V("OPERATOR_COMPOSE")),
            Pair("|>", V("OPERATOR_RCOMPOSE")),
            Pair("=>", V("OPERATOR_IMPL")),
            Pair("<=", V("OPERATOR_RIMPL")),
            Pair("<=>", V("OPERATOR_EQUIV")),
            Pair("\$member", LangBuiltin(Member)),
            Pair("\$extract", LangBuiltin(Extract)),
            Pair("\$count", LangBuiltin(Count)),
            Pair("\$min", LangBuiltin(MinBuiltin)),
            Pair("\$max", LangBuiltin(MaxBuiltin)),
            Pair("\$sum", LangBuiltin(SumBuiltin)),
            Pair("\$minimize", LangBuiltin(Minimize)),
            Pair("\$maximize", LangBuiltin(Maximize)),
            Pair("\$graph", LangBuiltin(Graph)),
            Pair("\$abs", LangBuiltin(AbsoluteValue)),
            Pair("\$sumBy", LangBuiltin(SumBy)),
            Pair("\$bind", LangBuiltin(SetBind)),
            Pair("\$true", LangBuiltin(TrueBuiltin))
    )

    fun builtin(operator: String): Exp<String> =
            ExpressionTransformer.duplicate(builtinMap.getOrElse(operator) { throw NotImplementedError("Operator $operator") })

    fun binaryOperator(op: Token, expr: List<FOIDPParser.Expr1Context>): Expression =
            Application(Application(builtin(op.text), ParseExp.visit(expr[0])), ParseExp.visit(expr[1]))
}
