package fms.parsers

import fms.fml.builtin.Assert
import fms.fml.builtin.SetBind
import fms.fml.expression.Application
import fms.fml.expression.BindPragma
import fms.fml.expression.Binding
import fms.fml.expression.Exp
import fms.fml.expression.Lambda
import fms.fml.expression.LangBuiltin
import fms.fml.expression.Let
import fms.fml.expression.SetExp
import org.antlr.v4.runtime.ParserRuleContext
import org.antlr.v4.runtime.tree.ParseTree

object ParseCommand : FOIDPParserBaseVisitor<Exp<String>>() {

    override fun visitSet(ctx: FOIDPParser.SetContext): Exp<String> {
        var out: Exp<String> = SetExp.make(ctx.expr().map { ParseExp.visit(it) })
        ctx.condition().asReversed().forEach { c ->
            when (c) {
                is FOIDPParser.BindCondContext -> {
                    val expr = ParseExp.visit(c.expr())
                    val pat = ParsePattern.visit(c.pattern())
                    out = Application.make(LangBuiltin(SetBind), expr, Lambda.make(pat, out))
                }
                is FOIDPParser.AssCondContext -> {
                    val expr = ParseExp.visit(c.expr())
                    val pat = ParsePattern.visit(c.pattern())
                    out = Let.make(listOf(Binding(pat, expr, null, BindPragma())), out)
                }
                is FOIDPParser.ExprCondContext -> {
                    val expr = ParseExp.visit(c.expr())
                    out = Application.make(LangBuiltin(Assert), expr, out)
                }
            }
        }
        return out
    }

    override fun visit(tree: ParseTree): Exp<String> {
        return super.visit(tree).apply { locate(tree as? ParserRuleContext) }
    }

    override fun defaultResult(): Exp<String> {
        throw Exception("No Default Parse Binding")
    }
}
