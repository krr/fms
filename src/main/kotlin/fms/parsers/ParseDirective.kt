package fms.parsers

import fms.optimiser.OptiSettings
import fms.translation.IDPCommand

object ParseDirective : FOIDPParserBaseVisitor<IDPCommand.() -> Unit>() {

    override fun visitDirective(ctx: FOIDPParser.DirectiveContext): IDPCommand.() -> Unit {
        val param = Integer.parseInt(ctx.INTLIT().text)
        when (ctx.ID().text) {
            "printDetail" -> return { trace.printDetail = param }
            "printASP" -> return { trace.printASP = (param != 0) }
            "printStats" -> return { trace.stats = (param != 0) }
            "nbModels" -> return { nbModels = param }
            "stdlib" -> return { stdLib = (param != 0) }
            "constrain" -> return { trace.constrain = (param != 0) }
            "inline" -> return { opt.inlineSingleUse = (param != 0) }
            "contantFolding" -> return { opt.constantFolding = (param != 0) }
            "checkType" -> return { trace.checkType = (param != 0) }
            "noOpt" -> return { opt = if (param == 0) OptiSettings.all else OptiSettings.none }
            "run" -> return { trace.runClingo = (param != 0) }
            "printDefault" -> return { trace.printDefault = (param != 0) }
        }
        throw Exception("Unknown Directive: ${ctx.ID().text}")
    }

    override fun visitScript(ctx: FOIDPParser.ScriptContext): IDPCommand.() -> Unit {
        return { scripts.add(ctx.text) }
    }

    override fun defaultResult(): IDPCommand.() -> Unit {
        throw Exception("No Default Parse Binding")
    }
}
