package fms.web

import fms.Versioning
import fms.terminal.runInput
import spark.Service
import spark.Service.ignite
import java.io.PrintWriter
import java.io.StringWriter

fun main(args: Array<String>) {
    println("Starting in Server Mode")
    run()
}

fun run(serverPort: Int = 4567) {
    val http: Service = ignite()

    http.apply {
        port(serverPort)
        staticFileLocation("/web")

        after(Cors.filter)
        get("/version") { _, _ ->
            "fms." + Versioning.buildNumber + "-" + Versioning.version
        }

        get("/git") { _, _ ->
            "fms." + Versioning.version
        }

        get("/date") { _, _ ->
            Versioning.gitDate
        }

        get("/build") { _, _ ->
            Versioning.buildDate
        }

        post("/eval") { req, res ->
            val str = StringWriter()
            runInput(req.body(), PrintWriter(str), PrintWriter(str))
            res.body(str.buffer.toString())
        }
    }
}
