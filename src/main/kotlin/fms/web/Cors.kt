package fms.web

import spark.Filter
import java.util.HashMap

object Cors {

    private val corsHeaders = HashMap<String, String>()

    init {
        corsHeaders.put("Access-Control-Allow-Methods", "GET,PUT,POST,DELETE,OPTIONS")
        corsHeaders.put("Access-Control-Allow-Origin", "*")
        corsHeaders.put("Access-Control-Allow-Headers", "Content-Type,Authorization,X-Requested-With,Content-Length,Accept,Origin,")
        corsHeaders.put("Access-Control-Allow-Credentials", "true")
    }

    val filter = Filter { _, response ->
        corsHeaders.forEach { key, value -> response.header(key, value) }
    }
}
