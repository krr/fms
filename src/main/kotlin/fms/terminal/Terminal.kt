package fms.terminal

import fms.fml.type.TypeException
import fms.translation.InvalidExpressionException
import org.jline.reader.LineReader
import org.jline.reader.LineReaderBuilder
import java.io.File
import java.io.PrintWriter

fun main(args: Array<String>) {
    val lRead = LineReaderBuilder.builder()
            .variable(LineReader.HISTORY_FILE, System.getProperty("java.io.tmpdir") + File.separator + "command-history")
            .build()
            .apply { setOpt(LineReader.Option.DISABLE_EVENT_EXPANSION) }
    val history = lRead.history
    val out = lRead.terminal.writer()
    try {

        while (true) {
            val r = lRead.readLine("IDP> ")
            runInput(r, out, out)
        }
    } catch (except: Exception) {
        out.println(except.message)
        out.println("Exiting IDP")
        out.flush()
        history.save()
    }
}

fun runInput(r: String, out: PrintWriter, err: PrintWriter) {
    try {
        val command = TerminalCommand.make(r, out)
        command.execute(out, err)
    } catch (exc: SyntaxErrorException) {
    } catch (exc: FreeVarsException) {
        out.println("No free variables allowed: ${exc.frees}")
    } catch (exc: InvalidExpressionException) {
        out.println("Expression could not be translated: \n${exc.explanation}")
    } catch (exc: TypeException) {
        out.println("Expression is badly typed: \n${exc.message}")
    } catch (exc: Exception) {
        out.println(exc.message)
        exc.printStackTrace()
    }
    return
}

class SyntaxErrorException : Exception()
class FreeVarsException(val frees: List<String>) : Exception("Did not expect free variables but got: $frees")
