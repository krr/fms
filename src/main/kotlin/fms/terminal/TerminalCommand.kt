package fms.terminal

import fms.fml.expression.Expression
import fms.fml.expression.Let
import fms.fml.type.InferType
import fms.parsers.ParseBinding
import fms.parsers.ParseTerminalCommand
import fms.parsers.parseString
import fms.translation.IDPCommand
import org.antlr.v4.runtime.BaseErrorListener
import org.antlr.v4.runtime.RecognitionException
import org.antlr.v4.runtime.Recognizer
import java.io.PrintWriter

sealed class TerminalCommand {
    companion object {
        fun make(str: String, errors: PrintWriter = PrintWriter(System.err)): TerminalCommand {
            val parser = parseString(str)
            parser.removeErrorListeners()
            parser.addErrorListener(object : BaseErrorListener() {
                override fun syntaxError(recognizer: Recognizer<*, *>?, offendingSymbol: Any?, line: Int, charPositionInLine: Int, msg: String?, e: RecognitionException?) {
                    errors.println("line $line:$charPositionInLine $msg")
                }
            })
            val foidpParse = parser.console()
            if (parser.numberOfSyntaxErrors > 0) {
                throw SyntaxErrorException()
            }
            return ParseTerminalCommand.visit(foidpParse)
        }
    }

    abstract fun execute(outStd: PrintWriter, outErr: PrintWriter)
}

data class TypeQuery(val exp: Expression) : TerminalCommand() {
    override fun execute(outStd: PrintWriter, outErr: PrintWriter) {
        println(InferType.typeCheck(Let.make(ParseBinding.parseLib("stdlib.idp4"), exp)))
    }
}

data class Execute(val command: IDPCommand) : TerminalCommand() {
    override fun execute(outStd: PrintWriter, outErr: PrintWriter) {
        val trans = command.translate()
        if (command.trace.printASP) {
            trans.writeProgram(outStd)
        }
        if (!command.trace.runClingo) {
            return
        }
        if (command.trace.printDetail == 2) {
            trans.executeRaw(outStd, outErr)
            outStd.flush()
            outErr.flush()
            return
        }

        val result = trans.execute()
        outStd.println(result.outputString())
        if (result.errorStream != "") {
            outStd.println("Clingo Error Stream:")
            outStd.println(result.errorStream)
        }
        outStd.flush()
    }
}

object Exit : TerminalCommand() {
    override fun execute(outStd: PrintWriter, outErr: PrintWriter) {
        System.exit(0)
    }
}

object Ignore : TerminalCommand() {
    override fun execute(outStd: PrintWriter, outErr: PrintWriter) {
    }
}
