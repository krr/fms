package fms

import java.io.IOException
import java.net.URL
import java.util.jar.Attributes
import java.util.jar.Manifest

object Versioning {
    // Class not from JAR
    val errorMessage = "Unknown (not running from JAR)"

    val version: String
        get() {
            return attrs.getValue("Git-Commit") ?: errorMessage
        }

    val buildDate: String
        get() {
            return attrs.getValue("Build-Date") ?: errorMessage
        }

    val gitDate: String
        get() {
            return attrs.getValue("Git-Committer-Date") ?: errorMessage
        }

    val buildNumber: String
        get() {
            return attrs.getValue("Build-Number") ?: errorMessage
        }

    val attrs: Attributes
        get() {
            val clazz = Versioning::class.java
            val className = clazz.simpleName + ".class"
            val classPath = clazz.getResource(className).toString()
            if (!classPath.startsWith("jar")) {
                Attributes()
            }
            val manifestPath = classPath.substring(0, classPath.lastIndexOf("!") + 1) + "/META-INF/MANIFEST.MF"
            var manifest: Manifest? = null
            try {
                manifest = Manifest(URL(manifestPath).openStream())
            } catch (e: IOException) {
                e.printStackTrace()
            }

            val attr = manifest!!.mainAttributes
            return attr
        }
}
