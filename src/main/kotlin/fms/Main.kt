package fms

import ch.qos.logback.classic.Level
import com.xenomachina.argparser.ArgParser
import com.xenomachina.argparser.default
import com.xenomachina.argparser.mainBody
import java.io.File
import java.io.PrintWriter

class MyArgs(parser: ArgParser) {
    val version by parser.flagging("--version", help = "print version number")
    val server by parser.flagging("--server", help = "runs IDP2ASP in web server mode")
    val port by parser.storing<Int>("--port", help = "Port of the web server") { toInt() }.default(4567)
    val verbose by parser.flagging("-v", "--verbose", help = "enable verbose mode")
    val spec by parser.positionalList(help = "filename of specification", sizeRange = 0..1)
}

fun verbose(lvl: Level) {
    (globalLog.underlyingLogger as ch.qos.logback.classic.Logger).level = lvl
}

fun readFile(name: String): File {
    val f = File(name)
    if (!f.exists()) {
        System.err.println("Invalid file path: $name")
        System.exit(1)
    }
    return f
}

fun main(args: Array<String>) = mainBody {
    val pargs = MyArgs(ArgParser(args))

    if (pargs.verbose) {
        verbose(Level.ALL)
        globalLog.trace { "Trace mode on" }
    } else {
        verbose(Level.INFO)
    }
    val spec = pargs.spec

    when {
        pargs.version -> println("fms." + Versioning.buildNumber + "-" + Versioning.version)
        pargs.server -> fms.web.run(pargs.port)
        spec.isEmpty() -> fms.terminal.main(args)
        else -> {
            val content = readFile(spec[0]).readText()
            fms.terminal.runInput(content, PrintWriter(System.out, true), PrintWriter(System.err, true))
            System.out.flush()
        }
    }
}
