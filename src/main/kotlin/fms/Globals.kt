package fms

import fms.fml.type.union
import mu.KLogger
import mu.KotlinLogging
import java.io.ByteArrayOutputStream
import java.io.PrintWriter

val globalLog: KLogger = KotlinLogging.logger("fms")

fun <E> List<List<E>>.concat(): List<E> =
        this.fold(listOf(), { a, b -> a + b })

fun ((PrintWriter) -> Unit).streamToString(): String {
    val os = ByteArrayOutputStream()
    val ps = PrintWriter(os)
    this(ps)
    ps.flush()
    return os.toString()
}

fun <A> Pair<List<A>, List<A>>.appendPair(): List<A> {
    return this.first + this.second
}

fun <A, B> Iterable<Map<A, B>>.unions(): Map<A, B> {
    return this.fold(emptyMap(), { a, b -> a.union(b) })
}