import fms.optimiser.rules.RecursiveApply
import fms.optimiser.rules.RuleSet
import fms.optimiser.rules.boolRules
import fms.parsers.asExpr
import org.amshove.kluent.shouldEqual
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.Parameterized

@RunWith(Parameterized::class)
class RuleVerification(val r: RuleSet, val input: String, val output: String) {

    companion object {
        @JvmStatic
        @Parameterized.Parameters(name = "[{1}] => [{2}]")
        fun data(): Any {
            return arrayOf(
                    arrayOf(boolRules, "~~ x", "x"),
                    arrayOf(boolRules, "a | ~~ x", "a | x"),
                    arrayOf(boolRules, "! {1..5} (\\x-> x < 3)", "~ (? {1..5} (\\x -> ~ (\\x -> x < 3) x))")
            )
        }
    }

    @Test
    fun test() {
        val inp = asExpr(input)
        val should = asExpr(output)
        val out = RecursiveApply(r).visit(inp)
        should shouldEqual out
    }
}