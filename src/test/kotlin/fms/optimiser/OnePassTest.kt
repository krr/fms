package fms.optimiser

import fms.parsers.asExpr
import org.amshove.kluent.shouldEqual
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.Parameterized

@RunWith(Parameterized::class)
class OnePassTest(val r: ExpressionTransformer, val input: String, val output: String) {

    companion object {
        @JvmStatic
        @Parameterized.Parameters(name = "[{1}] => [{2}]")
        fun data(): Any {
            return arrayOf(
                    arrayOf(ConstantFolder, "1+5", "6"),
                    arrayOf(ConstantFolder, "1+5*10", "51"),
                    arrayOf(ConstantFolder, "case 1 of 1 -> 10;", "10"),
                    arrayOf(ConstantFolder, "case 2 of 1 -> 10; 2 -> 20;", "20"),
                    arrayOf(ConstantFolder, "case 3 of 1 -> 1 ; 2 -> 2 ; _ -> 5; + 4", "9"),
                    arrayOf(ConstantFolder, "case (1,2) of (a,b) -> a + b;", "let a := 1 ; b := 2 in a + b"),
                    arrayOf(InlineLet, "let x := 5 in x + 4", "5 + 4")
            )
        }
    }

    @Test
    fun test() {
        val inp = asExpr(input)
        val should = asExpr(output)
        val out = r.visit(inp)
        should shouldEqual out
    }
}