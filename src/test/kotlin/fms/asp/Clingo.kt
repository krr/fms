package fms.asp

import org.amshove.kluent.`should be`
import org.junit.Test

class Clingo {

    @Test
    fun clingoInPath() {
        // Tests whether clingo is available
        try {
            val pb = ProcessBuilder("clingo")
            val process = pb.start()
            process.outputStream.close()
        } catch (e: Exception) {
            e `should be` null
        }
    }
}
