package fms.asp

import fms.streamToString
import org.amshove.kluent.`should equal`
import org.junit.Test
import toStream

class PrinterTest {
    @Test
    fun printPropositions() {
        val p = ClassLit(Sign.Positive, "p", listOf())
        val body = LitBody(ClassNaf(Sign.Positive, p))
        val head = Head.literal("q")
        val rule = Rule(head, listOf(body))

        "q:-p." `should equal` rule::toStream.streamToString()
    }

    @Test
    fun printCalculations() {
        val q = ClassLit(Sign.Positive, "q", listOf())
        val body1 = LitBody(ClassNaf(Sign.Positive, q))
        val body2 = LitBody(BuiltinNaf(Variable("X"), Binop.EQ, Arithm(IntTerm(1), ArithOp.ArPlus, IntTerm(2))))
        val head = Head.literal("p", listOf(Variable("X")))
        val rule = Rule(head, listOf(body1, body2))
        "p(X):-q,X=(1+2)." `should equal` rule::toStream.streamToString()
    }
}
