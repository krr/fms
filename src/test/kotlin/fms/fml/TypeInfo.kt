package fms.fml

import fms.parsers.asType
import org.amshove.kluent.`should be equal to`
import org.junit.Test

class TypeInfo {
    @Test
    fun arities() {
        asType("Int").arity `should be equal to` 0
        asType("a -> a").arity `should be equal to` 1
        asType("a -> a -> String").arity `should be equal to` 2
    }

    @Test
    fun set() {
        asType("Set Int").returnsSet() `should be equal to` true
        asType("Set Int -> Int").returnsSet() `should be equal to` false
        asType("Int -> Set Int").returnsSet() `should be equal to` true
    }
}