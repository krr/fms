package fms.fml

import fms.fml.type.InferType
import fms.fml.type.TypeException
import fms.parsers.asExpr
import org.amshove.kluent.AnyException
import org.amshove.kluent.shouldNotThrow
import org.amshove.kluent.shouldThrow
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.Parameterized

@RunWith(Parameterized::class)
class ShouldType(val wellTyped: Boolean, val expr: String, val text: String) {

    companion object {
        @JvmStatic
        @Parameterized.Parameters(name = "{2} ({1})")
        fun data(): Any {
            val bad = listOf(
                    "{5} 8", "\\x -> x x", "1 1", "let f x := 2 ; a := f 5 ; b := f \"hello\" in a b", "(\\f -> f 1 + f \"a\") (\\x -> x)", "{1, 1=1}", "{1 || 2}", "(\\x -> x) :: Int -> a", "let a := {1,a} in a"
            )
            val good = listOf(
                    "1", "(\\x -> x) (\\x -> x)", "let f x := 2 ; a := f 5 ; b := f \"hello\" in a + b", "let id x := x in id id", "! {1..5} \\x -> (\\x y -> x < y) x 2"
            )
            return bad.map { arrayOf(false, it, "BadlyTyped") } + good.map { arrayOf(true, it, "WellTyped") }
        }
    }

    @Test
    fun test() {
        val exp = asExpr(expr)
        val t = { InferType.typeCheck(exp) }
        if (wellTyped) {
            t shouldNotThrow AnyException
        } else {
            t shouldThrow TypeException::class
        }
    }
}
