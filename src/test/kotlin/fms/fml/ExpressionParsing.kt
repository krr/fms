package fms.fml

import fms.asp.ArithOp
import fms.asp.Binop
import fms.fml.expression.Alt
import fms.fml.expression.AnonymVar
import fms.fml.expression.Application
import fms.fml.expression.BindPragma
import fms.fml.expression.Binding
import fms.fml.expression.Case
import fms.fml.expression.Injected
import fms.fml.expression.Injection
import fms.fml.expression.Lambda
import fms.fml.expression.LangBuiltin
import fms.fml.expression.Let
import fms.fml.expression.V
import fms.fml.expression.VarPattern
import fms.fml.expression.VarT
import fms.parsers.ParseExp
import fms.parsers.ParseFile
import fms.parsers.parseStream
import fms.translation.SafeVariable
import org.amshove.kluent.`should be instance of`
import org.amshove.kluent.`should equal`
import org.junit.Test

class ExpressionParsing {
    @Test
    fun identityParsing() {
        val parser = parseStream("\\x->x.".byteInputStream())
        val exp = ParseExp.visit(parser.expr())
        exp `should equal` Lambda.make("x", V("x"))
    }

    @Test
    fun integerParsing() {
        val parser = parseStream("1.".byteInputStream())
        val exp = ParseExp.visit(parser.expr())
        exp `should equal` Injection<String, Int>(1)
    }

    @Test
    fun letParsing() {
        val parser = parseStream("let x := 1 in x.".byteInputStream())
        val exp = ParseExp.visit(parser.expr())
        exp `should equal` Let.make(listOf(Binding(VarPattern("x"), Injection(1), null, BindPragma())), V("x"))
    }

    @Test
    fun caseParsing() {
        val parser = parseStream("case x of 1 -> y; _ -> z;.".byteInputStream())
        val exp = ParseExp.visit(parser.expr())
        exp `should equal` Case(V("x"), listOf(Alt.make(Injected(1), V("y")), Alt.make(AnonymVar(), V("z"))))
    }

    @Test
    fun stringLit() {
        val parser = parseStream("\"hello world\"".byteInputStream())
        val exp = ParseExp.visit(parser.expr())
        exp `should equal` Injection<VarT, String>("hello world")
    }

    @Test
    fun literalSum() {
        val parser = parseStream("2-3".byteInputStream())
        val exp = ParseExp.visit(parser.expr())
        exp `should equal` Application.make(LangBuiltin<SafeVariable>(ArithOp.ArMinus), listOf(Injection(2), Injection(3)))
    }

    @Test
    fun literalCompare() {
        val parser = parseStream("2<3".byteInputStream())
        val exp = ParseExp.visit(parser.expr())
        exp `should equal` Application.make(LangBuiltin<SafeVariable>(Binop.LT), listOf(Injection(2), Injection(3)))
    }

    @Test
    fun patternLambda() {
        val parser = parseStream("\\(x,y) -> x".byteInputStream())
        val exp = ParseExp.visit(parser.expr())
        exp.`should be instance of`(Lambda::class)
        (exp as Lambda).f.unscope `should be instance of` Case::class
    }

    @Test
    fun assignmentFile() {
        val parser = parseStream("a := 3.".byteInputStream())
        ParseFile.visit(parser.foidpFile())
        // Should not throw exception
    }
}
