package fms.fml

import fms.fml.expression.Application
import fms.fml.expression.Bind
import fms.fml.expression.BindPragma
import fms.fml.expression.Lambda
import fms.fml.expression.Let
import fms.fml.expression.V
import fms.optimiser.OptiSettings
import fms.optimiser.optimise
import org.amshove.kluent.`should be empty`
import org.amshove.kluent.`should equal`
import org.amshove.kluent.`should not equal`
import org.amshove.kluent.shouldBe
import org.junit.Test

class ExpressionBuilding {

    @Test
    fun identity() {
        val vx = V("x")
        val vy = V("y")
        val identity = Lambda.make("x", vx)
        vy `should equal` identity.f.instantiate { vy }
        vx `should not equal` identity.f.instantiate { vy }
        identity `should equal` identity.f.instantiate { identity }
        identity.freeVars().`should be empty`()
        vx.freeVars() `should equal` listOf("x")

        val absIdentity = Lambda.make("x", identity)

        identity `should equal` absIdentity.f.instantiate { vx }
        identity `should equal` absIdentity.f.instantiate { identity }
    }

    @Test
    fun shadowing() {
        val vx = V("x")
        val b = Bind("x", vx.emptyScope(), null, BindPragma())
        val ax = Let(listOf(b), Application(vx, vx).abstractList(listOf("x")))

        ax.freeVars().size shouldBe 1
        val ax2 = optimise(ax, OptiSettings.none.copy(tarjanLet = true))
        ax2.freeVars().size shouldBe 1
    }
}
