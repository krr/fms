package fms.fml

import fms.fml.type.InferType
import fms.parsers.asExpr
import fms.parsers.asType
import org.amshove.kluent.shouldEqual
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.Parameterized

@RunWith(Parameterized::class)
class SpecificType(val expr: String, val type: String) {

    companion object {
        @JvmStatic
        @Parameterized.Parameters(name = "is {1}: {0}")
        fun data(): Any {
            val typed = listOf(
                    arrayOf("1", "Int"), arrayOf("\"h\"", "String"), arrayOf("\\x -> x", "a -> a"), arrayOf("\\x -> x + 1", "Int -> Int"), arrayOf("\\x -> \\y -> x", "a -> b -> a"), arrayOf("{}", "Set a"), arrayOf("{1}", "Set Int"), arrayOf("\$count {}", "Int"), arrayOf("\$sumBy (\\x -> x) {4}", "Int"), arrayOf("\$min {\"h\"}", "String"), arrayOf("\$min {}", "a"), arrayOf("{1..5}", "Set Int"), arrayOf("{1 || 1=1}", "Set Int"), arrayOf("\\x -> case x of 1 -> \"h\";", "Int -> String"), arrayOf("{x || x <- {1..5}}", "Set Int"), arrayOf("{x || x := 5}", "Set Int"), arrayOf("\\x -> (x :: Int)", "Int -> Int"), arrayOf("let tnum := \$min set ; set := {1..5} in tnum", "Int"), arrayOf("let iInt := id 1 ; iString := id \"h\" ; iFunc := id id ; id :: a -> a; id := (\\x -> x) in id", "a -> a"), arrayOf("let f () := 5 in f", "() -> Int"), arrayOf("let f _ := 5 in f", "a -> Int"), arrayOf("let a := case (1,\"a\") of (o,k) -> o; in a", "Int")
            )
            return typed
        }
    }

    @Test
    fun test() {
        val t1 = asType(type)
        val t2 = InferType.typeCheck(asExpr(expr))
        t1.canonical() shouldEqual t2.canonical()
    }
}
