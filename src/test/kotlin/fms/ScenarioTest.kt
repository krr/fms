import fms.translation.shouldSAT
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.Parameterized
import java.io.File

@RunWith(Parameterized::class)
class ScenarioTest(val scenario: File, val sat: Boolean) {
    companion object {
        @JvmStatic
        @Parameterized.Parameters(name = "{index}: {0} should SAT: {1} ")
        fun data(): Collection<Array<Any>> {
            val result = ArrayList<Array<Any>>()
            File("src/test/resources/sat_instances").listFiles().forEach { result.add(arrayOf(it, true)) }
            File("src/test/resources/unsat_instances").listFiles().forEach { result.add(arrayOf(it, false)) }
            return result
        }
    }

    @Test
    fun test() {
        shouldSAT(sat, scenario.readText())
    }
}
