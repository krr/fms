package fms.parsers

import org.amshove.kluent.`should be`
import org.antlr.v4.runtime.BailErrorStrategy
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.Parameterized

@RunWith(Parameterized::class)
class ParseIDPTest(val validSyntax: Boolean, val scenario: String) {
    companion object {
        @JvmStatic
        @Parameterized.Parameters(name = "{index}: Should be valid syntax {0}: {1} ")
        fun data(): Collection<Array<Any>> {
            val result = ArrayList<Array<Any>>()
            result.add(arrayOf(true, "1=1."))
            result.add(arrayOf(false, "linewithoutdot"))
            result.add(arrayOf(true, "//Some witty comment about Donald Trump"))
            result.add(arrayOf(true, "/* An even // funnier comment */ // about Donald /* Trump"))
            return result
        }
    }

    fun validFile(string: String): Boolean {
        val parser = parseStream(string.byteInputStream())
        parser.errorHandler = BailErrorStrategy()
        try {
            parser.foidpFile()
        } catch (e: Exception) {
            return false
        }
        return true
    }

    @Test
    fun test() {
        validSyntax `should be` validFile(scenario)
    }
}
