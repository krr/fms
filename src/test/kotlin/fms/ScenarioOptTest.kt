import fms.translation.test
import org.amshove.kluent.`should equal`
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.Parameterized
import java.io.File

@RunWith(Parameterized::class)
class ScenarioOptTest(val scenario: File) {
    companion object {
        @JvmStatic
        @Parameterized.Parameters(name = "{index}: {0}")
        fun data(): Collection<Array<Any>> {
            val result = ArrayList<Array<Any>>()
            File("src/test/resources/opt_instances").listFiles().forEach { result.add(arrayOf(it)) }
            return result
        }
    }

    @Test
    fun test() {
        val result = test(scenario.inputStream())
        val bestModel = result.witnesses.last().atoms
        bestModel["expected"] `should equal` bestModel["actual"]
    }
}
