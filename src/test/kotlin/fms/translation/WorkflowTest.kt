package fms.translation

import fms.asp.ClingoResult
import fms.parsers.ParseFile
import fms.parsers.parseStream
import org.amshove.kluent.`should be false`
import org.amshove.kluent.`should be true`
import org.antlr.v4.runtime.BailErrorStrategy
import org.junit.Test
import java.io.InputStream

fun test(stream: InputStream): ClingoResult {
    val parser = parseStream(stream)
    parser.errorHandler = BailErrorStrategy()

    val command = ParseFile.visit(parser.foidpFile())
    return command.translate().execute()
}

fun testExitCode(stream: InputStream): Int {
    return test(stream).exitCode
}

fun shouldSAT(b: Boolean, file: String) {
    val r = testExitCode(file.byteInputStream())
    when (r) {
        10 -> b.`should be true`() // Sat
        20 -> b.`should be false`() // Unsat
        30 -> b.`should be true`() // All models found
        else -> throw Exception("Unknown Clingo Exit Code: $r")
    }
}

class WorkflowTest {

    @Test
    fun satunsat() {
        shouldSAT(true, "1=1.")
        shouldSAT(false, "1=2.")
        shouldSAT(true, "1=2 | 3=3.")
        shouldSAT(false, "1=2 & 3=3.")
        shouldSAT(false, "1=2 <=> 3=3.")
        shouldSAT(true, "2<2 <=> 3>3.")
        shouldSAT(true, "a:=3.")
    }

    @Test
    fun letPatterns() {
        shouldSAT(true, "(a,b) := (4,5). a+b = 9.")
    }

    @Test
    fun import() {
        shouldSAT(true, "true.")
    }

    @Test
    fun nestedExist() {
        shouldSAT(true, "? {1,2} (\\x -> (? {1,2} (\\y -> x = y))).")
    }

    @Test
    fun recursiveDef() {
        shouldSAT(true, "fac x := case x of 0 -> 1 ; x -> x* fac (x-1);. fac 5 = 120.")
    }

    @Test
    fun integerPatterns() {
        shouldSAT(true, "f := \\x -> case x of < 0 -> x+1 ; > 0 -> x-1;. f 5 = 4.")
    }

    @Test
    fun herbrand() {
        shouldSAT(true, "s/1 :: constructor. nil/0 :: constructor. conv x := case x of s[a] -> 1 + conv a; nil[] -> 0;. 4 = conv (s(s (s(s (nil))))).")
        shouldSAT(false, "s/1 :: constructor. nil/0 :: constructor. conv x := case x of s[a] -> 1 + conv a; nil[] -> 0;. 5 = conv (s(s (s(s (nil))))).")
    }

    @Test
    fun setOperations() {
        shouldSAT(true, "sameset (union {1,2} {2,3}) {3,2,1}.")
        shouldSAT(true, "sameset (intersect {1,2} {2,3}) {2}.")
        shouldSAT(true, "sameset (setminus {1,2} {2,3}) {1}.")
    }
}
