package fms.translation

import fms.asp.ArithOp
import fms.asp.Arithm
import fms.asp.IntTerm
import fms.fml.expression.Application
import fms.fml.expression.Injection
import fms.fml.expression.Lambda
import fms.fml.expression.LangBuiltin
import fms.fml.expression.V
import fms.optimiser.OptiSettings
import fms.optimiser.optimise
import org.amshove.kluent.`should equal`
import org.junit.Test

class ExpressionTranslation {
    @Test
    fun constantJuggling() {
        val exp = Application(Lambda.make("x", V("x")), Injection(1))
        val trans = Translator(IDPCommand())
        val oExp = optimise(exp, OptiSettings.all)
        trans.visit(oExp.cast()) `should equal` SafeVariable(IntTerm(1))
    }

    @Test
    fun basicArithmetic() {
        val exp = Application.make(LangBuiltin<SafeVariable>(ArithOp.ArMinus), listOf(Injection(2), Injection(3)))
        val trans = Translator(IDPCommand())
        trans.visit(exp) `should equal` SafeVariable(Arithm(IntTerm(2), ArithOp.ArMinus, IntTerm(3)))
    }
}
