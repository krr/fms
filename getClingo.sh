#!/bin/sh

if [ ! -f build/clingo-5.2.2-linux-x86_64/clingo ]; then
    mkdir -p build
    cd build
    wget -N https://github.com/potassco/clingo/releases/download/v5.2.2/clingo-5.2.2-linux-x86_64.tar.gz
    tar -xf clingo-5.2.2-linux-x86_64.tar.gz
    cd ..
else
    echo "Clingo already in cache"
fi
