# Functional Modelling System

[![pipeline status](https://gitlab.com/krr/fms/badges/master/pipeline.svg)](https://gitlab.com/krr/fms/commits/master)
[![coverage report](https://gitlab.com/krr/fms/badges/master/coverage.svg)](https://gitlab.com/krr/fms/commits/master)
[![codebeat badge](https://codebeat.co/badges/cde9edfd-0a79-4809-87b0-556084f33bd3)](https://codebeat.co/projects/gitlab-com-krr-fms-master)

Online **playground** at: https://tech.io/playgrounds/12240/

Latest **build** available at: https://krr.gitlab.io/fms/libs/fms.jar

Latest **javadoc** available at: https://krr.gitlab.io/fms/javadoc

Latest **test report** available at: https://krr.gitlab.io/fms/reports/tests/test/

Latest **coverage report** available at: https://krr.gitlab.io/fms/reports/coverage/

**Source** available at: https://gitlab.com/krr/fms