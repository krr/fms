#!/bin/sh

export PATH=$PATH:`pwd`/build/clingo-5.2.2-linux-x86_64
mkdir -p build
cd build
wget --quiet https://github.com/potassco/clingo/releases/download/v5.2.2/clingo-5.2.2-linux-x86_64.tar.gz
tar -xf clingo-5.2.2-linux-x86_64.tar.gz
cd ..
java -jar build/libs/fms.jar --server --port $PORT
